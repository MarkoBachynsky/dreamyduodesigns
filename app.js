
const cors = require('cors');
var express = require('express');
var session = require('express-session');
const FS = require( 'fs' ); // File Saving / Loading

// var bodyParser = require('body-parser');
var path = require('path');
var dotenv = require('dotenv');
dotenv.config();
// console.log(process.env.STRIPE_KEY);
const STRIPE = require('stripe')(process.env.STRIPE_KEY);
const cookieParser = require('cookie-parser');
const DBService = require('./dbService');
// no cache saving
// const nocache = require('nocache');

 
// express object 
var app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
 
// set public folder as root 
app.use(express.static(path.join(__dirname, 'public')));
 
// setup Cross-Origin Resource Sharing (CORS) 
// origin: allow data from anywhere 
// credentials: allow cookies 
 
app.use( 
	cors( 
		{ 
			origin: "*", 
			credentials: true 
		} 
	) 
);
 
// session config 
app.use(session( 
	{ 
		secret: 'secret', 
		resave: true, 
		saveUninitialized: true 
	}));
 
// setup cookieparser 
app.use(cookieParser(process.env.KEY));

// no cache
// app.use(nocache());
// no ETag
app.set('etag', false);

// view engine setup 
app.set('views', path.join(__dirname,'/public/views'));
app.set('view engine', 'ejs');
 
// website root index route  
app.get('/', ((request, response) => 
{ 
	write_log("\n" + "route(/) ");
	// read cookies 
	// write_log("Cookies: ", request.cookies) 
 
	// Cookies that have been signed 
	// write_log("Signed Cookies: ", request.signedCookies) 
 
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
		if (account_attributes.isAdmin == 1)
		{
			response.redirect('admin');
		}
		else 
		{ 
			response.redirect('shirts');
		}
	})
	.catch(() => 
	{ 
		write_log("route(/) \tRESULT.catch()");
		write_log("route(/) \tif loggedIn == false");
		response.redirect('/login');
	});
}));  

 
// Route to Login Page 
app.get('/login', (request, response) => 
{ 
	write_log("\n" + "route(/login)");
 
	// expire the cookies 
	var options = 
	{ 
		maxAge: 1000 * 60 * 0, // Would expire after 0.0 hours  
		httpOnly: false, // The cookie only accessible by the web server 
		signed: false // Indicates if the cookie should be signed 
	} 
 
	// Set cookie 
	response.cookie('email', "", options) // options is optional 
	response.cookie('password', "", options) // options is optional 
 
	response.render('login');
});

// Route to register Page 
app.get('/register', (request, response) => 
{ 
	write_log("\n" + "route(/register)");
	response.render('register');
});

// Route to ForgotPassword Page 
app.get('/forgotPassword', (request, response) => 
{ 
	write_log("\n" + "route(/forgot-password)");
 
	// expire the cookies 
	var options = 
	{ 
		maxAge: 1000 * 60 * 0, // Would expire after 0.0 hours  
		httpOnly: false, // The cookie only accessible by the web server 
		signed: false // Indicates if the cookie should be signed 
	} 
 
	// Set cookie 
	response.cookie('email', "", options) // options is optional 
	response.cookie('password', "", options) // options is optional 
 
	response.render('forgot-password');
});

// Route to verify Page 
app.get('/verify', (request, response) => 
{
	write_log("\n" + "route(/verify) ");
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
		// write_log('account_attributes');
		// console.table(account_attributes);

		// only send basic attributes
		account_attributes = 
		{
			name: 					account_attributes.name,
			cart:					account_attributes.cart,
			cart_points:			account_attributes.cart_points,
			date_lastOrderPlaced:	account_attributes.date_lastOrderPlaced,
			date_last_visited:		account_attributes.date_last_visited,
			email_verified:	account_attributes.email_verified
		};
		response.render('verify',
		{
			account_attributes: account_attributes
		});
		
	})
	.catch((error) =>
	{
		// write_log("route(/verify) \tRESULT.catch()");
		// write_log("route(/verify) \tif loggedIn == false");
		// write_log(error);
		response.redirect('/login');
	});
});

// Route to account Page 
app.get('/account', (request, response) => 
{
	write_log("\n" + "route(/account) ");
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
		// write_log('account_attributes');
		// console.table(account_attributes);

		// only send basic attributes
		account_attributes = 
		{
			name: 					account_attributes.name,
			email: 					account_attributes.email,
			cart:					account_attributes.cart,
			cart_points:			account_attributes.cart_points,
			date_lastOrderPlaced:	account_attributes.date_lastOrderPlaced,
			date_last_visited:		account_attributes.date_last_visited,
			email_verified:	account_attributes.email_verified
		};
		response.render('account',
		{
			account_attributes: account_attributes
		});
		
	})
	.catch((error) =>
	{
		// write_log("route(/account) \tRESULT.catch()");
		// write_log("route(/account) \tif loggedIn == false");
		// write_log(error);
		response.redirect('/login');
	});
});

app.patch('/forgotPasswordGenerateCode', ( request, response ) =>
    {
        write_log( "\n" + "route(/forgotPasswordGenerateCode)" );
        const email = request.body.email;

        const DB = DBService.getDbServiceInstance();
        const RESULT = DB.forgotPasswordGenerateCode( email );

        RESULT.then( data =>
        {
            response.json( { data: data } );
        } )
            .catch( ( error ) =>
            {
                write_log( "route(/forgotPasswordGenerateCode) \tRESULT.catch()" );
                write_log( error );
                response.json( false );
            } );
    });

app.patch('/send_verification_code', ( request, response ) =>
    {
        write_log( "\n" + "route(/send_verification_code)" );
        var loggedInResponse = checkIfLoggedIn( request );
        loggedInResponse.then( ( account_attributes ) =>
        {
            const DB = DBService.getDbServiceInstance();
            const RESULT = DB.send_verification_code( account_attributes );
            RESULT.then( data =>
            {
                response.json( true );
            } )
                .catch( ( error ) =>
                {
                    write_log( "route(/send_verification_code) \tRESULT.catch()" );
                    write_log( error );
                    response.json( false );
                } );

        } )
            .catch( () =>
            {
                write_log( "route(/send_verification_code) \tRESULT.catch()" );
                write_log( "route(/send_verification_code) \tif loggedIn == false" );
                response.redirect( '/login' );
            } );
    });

app.patch('/updateAccountAttributes', ( request, response ) =>
    {
        write_log( "\n" + "route(/updateAccountAttributes)" );
        var loggedInResponse = checkIfLoggedIn( request );
        loggedInResponse.then( ( account_attributes ) =>
        {
            const new_email = request.body.new_email;
            const new_name = request.body.new_name;

            const DB = DBService.getDbServiceInstance();
            const RESULT = DB.updateAccountAttributes( account_attributes, new_email, new_name );

            RESULT.then( data =>
            {
                var options = {
                    maxAge: 1000 * 60 * 20160, // Would expire after two weeks (20160 minutes)
                    httpOnly: false, // The cookie only accessible by the web server 
                    signed: false // Indicates if the cookie should be signed 
                };

                // Set cookie 
                response.cookie( 'email', new_email, options ); // options is optional 
                // response
                response.json( true );
            })
            .catch( ( error ) =>
            {
                write_log( "route(/updateAccountAttributes) \tRESULT.catch()" );
                write_log( error );
                response.json( error );
            });
        })
        .catch( () =>
        {
            write_log( "route(/updateAccountAttributes) \tRESULT.catch()" );
            write_log( "route(/updateAccountAttributes) \tif loggedIn == false" );
            response.redirect( '/login' );
        } );
    });

app.patch('/updatePassword', ( request, response ) =>
    {
        write_log( "\n" + "route(/updatePassword)" );
        const email = request.body.email;
        const password = request.body.password;
        const verificationCode = request.body.verificationCode;

        write_log( email );
        write_log( password );
        write_log( verificationCode );

        const DB = DBService.getDbServiceInstance();
        const RESULT = DB.updatePassword( email, password, verificationCode );

        RESULT.then( data =>
        {
            response.json( { data: data } );
        } )
        .catch( ( error ) =>
        {
            write_log( "route(/updatePassword) \tRESULT.catch()" );
            write_log( error );
            response.json( false );
        } );
    });

app.patch('/submit_verification_code', ( request, response ) =>
    {
        write_log( "\n" + "route(/submit_verification_code)" );
        var loggedInResponse = checkIfLoggedIn( request );
        loggedInResponse.then( ( account_attributes ) =>
        {
            const verification_code = request.body.verification_code;

            const DB = DBService.getDbServiceInstance();
            const RESULT = DB.submit_verification_code( account_attributes, verification_code );

            RESULT.then( data =>
            {
                response.json( true );
            } )
                .catch( ( error ) =>
                {
                    write_log( "route(/submit_verification_code) \tRESULT.catch()" );
                    write_log( error );
                    response.json( false );
                } );
        } )
            .catch( () =>
            {
                write_log( "route(/submit_verification_code) \tRESULT.catch()" );
                write_log( "route(/submit_verification_code) \tif loggedIn == false" );
                response.redirect( '/login' );
            } );

    });

// render 
app.get('/menu', ( request, response ) =>
    {
        write_log( "\n" + "route(/menu)" );
        var loggedInResponse = checkIfLoggedIn( request );
        loggedInResponse.then( ( account_attributes ) =>
        {
            // write_log(account_attributes);
            // console.table(account_attributes);
            // only send basic attributes
            account_attributes =
            {
                name: account_attributes.name,
                cart: account_attributes.cart,
                cart_points: account_attributes.cart_points,
                date_lastOrderPlaced: account_attributes.date_lastOrderPlaced,
                date_last_visited: account_attributes.date_last_visited,
                email_verified: account_attributes.email_verified
            };
            response.render( 'menu',
                {
                    account_attributes: account_attributes
                } );

        } )
            .catch( () =>
            {
                write_log( "route(/menu) \tRESULT.catch()" );
                write_log( "route(/menu) \tif loggedIn == false" );
                response.redirect( '/login' );
            } );
    });

// render 
app.get('/shirts', ( request, response ) =>
    {
        write_log( "\n" + "route(/shirts)" );
        var loggedInResponse = checkIfLoggedIn( request );
        loggedInResponse.then( ( account_attributes ) =>
        {
            // write_log(account_attributes);
            // console.table(account_attributes);
            // only send basic attributes
            account_attributes =
            {
                name: account_attributes.name,
                cart: account_attributes.cart,
                cart_points: account_attributes.cart_points,
                date_lastOrderPlaced: account_attributes.date_lastOrderPlaced,
                date_last_visited: account_attributes.date_last_visited,
                email_verified: account_attributes.email_verified
            };
            response.render( 'shirts',
                {
                    account_attributes: account_attributes
                } );

        } )
            .catch( () =>
            {
                write_log( "route(/shirts) \tRESULT.catch()" );
                write_log( "route(/shirts) \tif loggedIn == false" );
                response.redirect( '/login' );
            } );
    });

// render dynamic product page
app.get('/shirt/*', ((request, response, next) => 
{
    // write_log(`route${request.url}`);
    let string_url_route = request.url.substring(request.url.lastIndexOf('/') + 1);
    // write_log(`string_url_route(${string_url_route})`);
    
	const DB = DBService.getDbServiceInstance();
	const RESULT = DB.get_table_shirts();
 
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{ 
		RESULT.then(data =>  
		{ 
            let array_objects_shirts = [];
            let map_shirts = new Map();
            Array.from(data).forEach(( { id, name, product, style, color, size, discount, price, stock } ) =>
            {
                let string_name_url = name.replace( / /g, '_' );

                // if map does not have unique shirt name, add it
                if ( !map_shirts.has( string_name_url ) )
                {
                    let object_shirt_properties = {
                        int_id: id,
                        string_name: name,
                        string_product: product,
                        string_style: style,
                        string_color: color,
                        string_size: size,
                        int_discount: discount,
                        decimal_price: price,
                        int_stock: stock
                    };
                    map_shirts.set( string_name_url, [ object_shirt_properties ] );
                }

                else
                {
                    let array_object_existing_shirt = map_shirts.get( string_name_url );
                    let object_shirt_properties = {
                        int_id: id,
                        string_name: name,
                        string_product: product,
                        string_style: style,
                        string_color: color,
                        string_size: size,
                        int_discount: discount,
                        decimal_price: price,
                        int_stock: stock
                    };
                    array_object_existing_shirt.push( object_shirt_properties );
                }
            });

            // console.log(map_shirts);

            if (map_shirts.has(string_url_route))
            {
                write_log(`route(/*)`);
                write_log(`route(${string_url_route})`);
        
                array_objects_shirts = map_shirts.get(string_url_route);
                // console.log(`route(${string_url_route})`);
                // console.log(array_objects_shirts);

                response.render('product',
                {
                    account_attributes: account_attributes,
                    array_objects_shirts: array_objects_shirts
                });
            }
            else
            {
                next();
            }
            
		})
		.catch(err => write_log(err));
	}) 
	.catch(() => 
	{ 
		write_log("route(/get_table_shirts) \tRESULT.catch()");
		write_log("route(/get_table_shirts) \tif loggedIn == false");
		response.redirect('/login');
	});
}));  

// render 
// app.get('/rewards', function (request, response) 
// { 
// 	write_log("\n" + "route(/rewards)");
// 	var loggedInResponse = checkIfLoggedIn(request);
// 	loggedInResponse.then((account_attributes) => 
// 	{

	// 		// write_log(account_attributes);
	// 		// console.table(account_attributes);


			// // only send basic attributes
			// account_attributes = 
			// {
			// 	name: 					account_attributes.name,
			// 	cart:					account_attributes.cart,
			// 	cart_points:			account_attributes.cart_points,
			// 	date_lastOrderPlaced:	account_attributes.date_lastOrderPlaced,
			// 	date_last_visited:		account_attributes.date_last_visited,
			// 	email_verified:	account_attributes.email_verified
			// };
			// response.render('rewards',
			// {
			// 	account_attributes: account_attributes
			// });
// 	})
// 	.catch(() => 
// 	{ 
// 		write_log("route(/rewards) \tRESULT.catch()");
// 		write_log("route(/rewards) \tif loggedIn == false");
// 		response.redirect('/login');
// 	});
// });

app.post('/auth', ( request, response ) =>
    {
        write_log( "\n" + "route(/auth)" );

        try
        {
            // check  
            const email = request.body.email;
            const password = request.body.password;

            // write_log("email, password: ");
            // write_log(email);
            // write_log(password);
            const DB = DBService.getDbServiceInstance();
            const RESULT = DB.getUserData( email, password );
            RESULT.then( RESULTs =>
            {
                let options = {
                    maxAge: 1000 * 60 * 20160, // Would expire after two weeks (20160 minutes)
                    httpOnly: false, // The cookie only accessible by the web server 
                    signed: false // Indicates if the cookie should be signed 
                };

                // Set cookie 
                response.cookie( 'email', email, options ); // options is optional 
                response.cookie( 'password', password, options ); // options is optional 



                // write_log("Cookies created: email, password");
                // response.redirect('/menu');
                response.json( true );

            } ).catch( ( dataRESULT ) =>
            {
                write_log( "route(/auth) \tRESULT.catch()" );
                write_log( dataRESULT );
                var options = {
                    maxAge: 1000 * 60 * 0, // Would expire after 0.0 hours  
                    httpOnly: false, // The cookie only accessible by the web server 
                    signed: false // Indicates if the cookie should be signed 
                };

                // Set cookie 
                response.cookie( 'email', email, options ); // options is optional 
                response.cookie( 'password', password, options ); // options is optional 
                response.json( false );
            } );
        }
        catch ( error )
        {
            write_log( "route(/auth) \tError:" + error );
            response.redirect( '/login' );
        }
    });

// read 
app.get('/get_menu', (request, response) => 
{ 
	// write_log("\n" + "route(/get_menu) ");
	const DB = DBService.getDbServiceInstance();
	const RESULT = DB.get_menu();
 
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{ 
		RESULT.then(data =>  
		{ 
			// write_log(data);
			response.json({ data: data });
		})
		.catch(err => write_log(err));
	}) 
	.catch(() => 
	{ 
		write_log("route(/get_menu) \tRESULT.catch()");
		write_log("route(/get_menu) \tif loggedIn == false");
		response.redirect('/login');
	});
});

// read 
app.get('/get_table_shirts', (request, response) => 
{ 
	// write_log("\n" + "route(/get_table_shirts) ");
	const DB = DBService.getDbServiceInstance();
	const RESULT = DB.get_table_shirts();
 
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{ 
		RESULT.then(data =>  
		{ 
			// write_log(data);
			response.json({ data: data });
		})
		.catch(err => write_log(err));
	}) 
	.catch(() => 
	{ 
		write_log("route(/get_table_shirts) \tRESULT.catch()");
		write_log("route(/get_table_shirts) \tif loggedIn == false");
		response.redirect('/login');
	});
});

// render 
app.get('/checkout', (request, response) => 
{ 
	write_log("\n" + "route(/checkout)");
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
		// only send basic attributes
		account_attributes = 
		{
			name: 					account_attributes.name,
			cart:					account_attributes.cart,
			cart_points:			account_attributes.cart_points,
			date_lastOrderPlaced:	account_attributes.date_lastOrderPlaced,
			date_last_visited:		account_attributes.date_last_visited,
			email_verified:	account_attributes.email_verified
		};
		response.render('checkout',
		{
			account_attributes: account_attributes
		});
		
	})
	.catch(() => 
	{ 
		write_log("route(/) \tRESULT.catch()");
		write_log("route(/) \tif loggedIn == false");
		response.redirect('/login');
	});
});
 
// render 
app.get('/orders', (request, response) => 
{ 
	write_log("\n" + "route(/orders)");
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
		// only send basic attributes
		account_attributes = 
		{
			name: 					account_attributes.name,
			cart:					account_attributes.cart,
			cart_points:			account_attributes.cart_points,
			date_lastOrderPlaced:	account_attributes.date_lastOrderPlaced,
			date_last_visited:		account_attributes.date_last_visited,
			email_verified:	account_attributes.email_verified
		};
		response.render('orders',
		{
			account_attributes: account_attributes
		});
		
	}) 
	.catch(() => 
	{ 
		write_log("route(/orders) \tRESULT.catch()");
		write_log("route(/orders) \tif loggedIn == false");
		response.redirect('/login');
	});
});

// read 
app.get('/getUserData', (request, response) => 
{ 
	// write_log("\n"+ "route(/getUserData) ");
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{ 
		const email = request.cookies.email;
		const password = request.cookies.password;
		const DB = DBService.getDbServiceInstance();
		const RESULT = DB.getUserData(email, password);
		RESULT 
		.then(data =>  
		{ 
			response.json({ data: data });
		})
		.catch(err => write_log(err));
	}) 
	.catch(() => 
	{ 
		write_log("route(/getUserData) \tRESULT.catch()");
		write_log("route(/getUserData) \tif loggedIn == false");
		response.redirect('/login');
	});
});

// read 
app.get('/get_user_orders', (request, response) => 
{ 
	// write_log("\n"+ "route(/get_user_orders) ");
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
		const DB = DBService.getDbServiceInstance();
		const RESULT = DB.get_user_orders(account_attributes.id);
	 
		RESULT.then(data =>  
		{ 
			response.json(data);
		})
		.catch(err =>
		{
			write_log(err)
			response.json(null);
		});
	})
	.catch(() => 
	{ 
		// write_log("route(/get_user_orders) \tRESULT.catch()");
		write_log("route(/get_user_orders) \tif loggedIn == false");
		response.redirect('/login');
	});
});

// modify  
app.patch('/admin_get_orders', (request, response) => 
{
	// write_log("/admin_get_orders");
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
		// write_log("admin_get_orders(/) \tRESULT.then()");
		if (account_attributes.isAdmin == 1) 
		{ 
			const { search_limit } = request.body;

			// console.table(['search_limit',search_limit]);
			// write_log("/admin_get_orders ADMIN");
			const DB = DBService.getDbServiceInstance();
			const RESULT = DB.admin_get_orders(search_limit);
			
			RESULT.then(data =>  
			{ 
				response.json(data);
			}) 
			.catch((err) =>
			{
				write_log("/admin_get_orders Catch Error");
				write_log(err)
			});			
		} 
		else 
		{ 
			write_log("/admin_get_orders ADMIN FALSE");
			response.end();
		} 
	})
	.catch(() => 
	{
		// write_log("route(/) \tRESULT.catch()");
		write_log("route(/) \tif loggedIn == false");
		response.redirect('/login');
	});
});

// read  
app.get('/admin_get_admin_config', (request, response) => 
{
	write_log("/admin_get_admin_config");
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) =>
	{
		write_log("admin(/) \tRESULT.then()");
		if (account_attributes.isAdmin == 1) 
		{
			write_log("/admin_get_admin_config ADMIN TRUE");
			const DB = DBService.getDbServiceInstance();
			const RESULT = DB.admin_get_admin_config();
			
			RESULT.then(data =>  
			{
                console.log(data);
				response.json(data);
			})
			.catch(err => write_log(err));			
		}
		else 
		{
			write_log("/admin_get_admin_config ADMIN FALSE");
			response.end();
		}
	})
	.catch(() => 
	{
		write_log("route(/admin_get_admin_config) \tif loggedIn == false");
		response.redirect('/login');
	});
});

// read 
app.get('/adminGetAccessCodes', (request, response) => 
{  
	write_log("/adminGetAccessCodes");
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
		write_log("admin(/) \tRESULT.then()");
		if (account_attributes.isAdmin == 1) 
		{ 
			write_log("/adminGetAccessCodes ADMIN TRUE");
			const DB = DBService.getDbServiceInstance();
			const RESULT = DB.adminGetAccessCodes();
			
			RESULT.then(data =>  
			{
				response.json({ data: data });
			})
			.catch(err => write_log(err));			
		} 
		else 
		{ 
			write_log("/adminGetAccessCodes ADMIN FALSE");
			response.end();
		} 
	})
	.catch(() => 
	{ 
		write_log("route(/) \tRESULT.catch()");
		write_log("route(/) \tif loggedIn == false");
		response.redirect('/login');
	});
});


// post 
app.post('/generateAccessCodes', (request, response) => 
{  
	write_log("/generateAccessCodes");
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
		write_log("admin(/) \tRESULT.then()");
		if (account_attributes.isAdmin == 1) 
		{ 
			write_log("/generateAccessCodes ADMIN TRUE");
			const DB = DBService.getDbServiceInstance();
			const RESULT = DB.generateAccessCodes(10);
			
			RESULT.then(data =>  
			{
				response.json({ data: data });
			})
			.catch(err => write_log("/admin error \n" + err));		
		} 
		else 
		{ 
			write_log("/generateAccessCodes ADMIN FALSE");
			response.end();
		} 
	})
	.catch(() => 
	{ 
		write_log("route(/generateAccessCodes) \tRESULT.catch()");
		write_log("route(/generateAccessCodes) \tif loggedIn == false");
		response.redirect('/login');
	});
});
 
// render 
app.get('/admin', (request, response) => 
{ 
	write_log("\n" + "route(/admin) ");
 
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
		write_log("admin(/) \tRESULT.then()");
		if (account_attributes.isAdmin == 1) 
		{ 
			// only send basic attributes
			account_attributes = 
			{
				name: 					account_attributes.name,
				cart:					account_attributes.cart,
				cart_points:			account_attributes.cart_points,
				date_lastOrderPlaced:	account_attributes.date_lastOrderPlaced,
				date_last_visited:		account_attributes.date_last_visited,
				email_verified:			account_attributes.email_verified
			};
			response.render('admin',
			{
				account_attributes: account_attributes
			});
		} 
		else 
		{ 
			response.redirect('/');
		}
		
	})
	.catch(() => 
	{ 
		write_log("route(/admin) \tRESULT.catch()");
		write_log("route(/admin) \tif loggedIn == false");
		response.redirect('/login');
	});
});

// read 
app.get('/help', (request, response) => 
{ 
	write_log("\n" + "route(/help) ");
 
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{ 
		write_log("help(/) \tRESULT.then()");
		// only send basic attributes
		account_attributes = 
		{
			name: 					account_attributes.name,
			cart:					account_attributes.cart,
			cart_points:			account_attributes.cart_points,
			date_lastOrderPlaced:	account_attributes.date_lastOrderPlaced,
			date_last_visited:		account_attributes.date_last_visited,
			email_verified:	account_attributes.email_verified
		};
		response.render('help',
		{
			account_attributes: account_attributes
		});
		
	})
	.catch(() => 
	{ 
		// write_log("route(/) \tRESULT.catch()");
		write_log("route(/help) \tif loggedIn == false");
		response.redirect('/login');
	});
});

// read 
app.get('/feedback', (request, response) => 
{ 
	write_log("\n" + "route(/feedback) ");

	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) =>
	{
		write_log("feedback(/) \tRESULT.then()");

		// only send basic attributes
		account_attributes = 
		{
			name: 					account_attributes.name,
			cart:					account_attributes.cart,
			cart_points:			account_attributes.cart_points,
			date_lastOrderPlaced:	account_attributes.date_lastOrderPlaced,
			date_last_visited:		account_attributes.date_last_visited,
			email_verified:	account_attributes.email_verified
		};
		response.render('feedback',
		{
			account_attributes: account_attributes
		});
		
	})
	.catch(() =>
	{
		write_log("route(/feedback) \tRESULT.catch()");
		write_log("route(/feedback) \tif loggedIn == false");
		response.redirect('/login');
	});
});
 
// update 
app.patch('/cartAddItem', (request, response) => 
{
	write_log("\n"+ "route(/cartAddItem) ");
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
		const { itemId, itemQty } = request.body;
		const DB = DBService.getDbServiceInstance();
		const RESULT = DB.cartAddItem(account_attributes, itemId, itemQty);
	 
		RESULT.then(data => 
		{ 
			// write_log("\n" + "route(/cartAddItem) \t RESULTS:");
            // console.log('DATA')
            // console.log(data);
			response.json(data);
		}) 
		.catch(err => write_log(err));
	})
	.catch(() => 
	{ 
		write_log("route(/cartAddItem) \tif loggedIn == false");
		response.redirect('/login');
	});
});

// update 
app.patch('/cartSubtractItem', (request, response) => 
{ 
	// write_log("\n"+ "route(/cartSubtractItem) ");
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
		const { itemId, itemQty } = request.body;
		const DB = DBService.getDbServiceInstance();
		const RESULT = DB.cartSubtractItem(account_attributes, itemId, itemQty);
	 
		RESULT.then(data => 
		{ 
			// write_log("\n" + "route(/cartSubtractItem) \t RESULTS:");
			response.json(data);
		})
		.catch(err => write_log(err));
	})
	.catch(() => 
	{ 
		// write_log("route(/cartSubtractItem) \tRESULT.catch()");
		write_log("route(/cartSubtractItem) \tif loggedIn == false");
		response.redirect('/login');
	});
});

// removes all items from cart AND cart_points
// patch 
app.patch('/cartRemoveAllItems', (request, response) => 
{
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{ 
		// write_log("\n"+ "route(/cartRemoveAllItems) ");
		const DB = DBService.getDbServiceInstance();
		const RESULT = DB.cartRemoveAllItems(account_attributes);
	
		RESULT.then(data => 
		{ 
			// write_log("\n" + "route(/cartRemoveAllItems) \t RESULTS:");
			response.json({ data: data });
		}) 
		.catch(err => write_log(err));
	})
	.catch(() => 
	{
		// user is not logged in
		// write_log("route(/cartRemoveAllItems) \tRESULT.catch()");
		write_log("route(/cartRemoveAllItems) \tif loggedIn == false");
		response.redirect('/login');
	});
});

// update 
app.patch('/setCartPointsData', (request, response) => 
{
	// write_log("\n"+ "route(/setCartPointsData) ");
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
		const email		= request.cookies.email;
		const password	= request.cookies.password;
		var { itemId }	= request.body;
		var itemQty 	= 1;
		const DB = DBService.getDbServiceInstance();

		// create user cart object
		var cart = [[itemId, itemQty]];
		var jsonObject =
		{
			cart: cart // jsonName : values
		};
		jsonObject = JSON.stringify(jsonObject);

		const RESULT = DB.setCartPointsData(email, password, jsonObject);
		RESULT.then(data => 
		{ 
			// write_log("\n" + "route(/setCartPointsData) \t RESULTS:");
			response.json({ data: data });
		}) 
		.catch(err => write_log(err));
	})
	.catch(() => 
	{ 
		write_log("route(/setCartPointsData) \tRESULT.catch()");
		write_log("route(/setCartPointsData) \tif loggedIn == false");
		response.redirect('/login');
	});
});

// patch 
app.patch('/cartPointsRemoveAllItems', (request, response) => 
{
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{ 
		// write_log("\n"+ "route(/cartPointsRemoveAllItems) ");
		const email = request.cookies.email;
		const password = request.cookies.password;
		const DB = DBService.getDbServiceInstance();
		const RESULT = DB.cartPointsRemoveAllItems(email, password);
	
		RESULT.then(data => 
		{ 
			// write_log("\n" + "route(/cartPointsRemoveAllItems) \t RESULTS:");
			response.json({ data: data });
		}) 
		.catch(err => write_log(err));
	})
	.catch(() => 
	{
		// user is not logged in
		write_log("route(/cartPointsRemoveAllItems) \tRESULT.catch()");
		// write_log("route(/cartPointsRemoveAllItems) \tif loggedIn == false");
		response.redirect('/login');
	});
});

// patch 
app.patch('/cartPointsRemoveAllItems', (request, response) => 
{
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{ 
		write_log("\n"+ "route(/cartPointsRemoveAllItems) ");
		const email = request.cookies.email;
		const password = request.cookies.password;
		const DB = DBService.getDbServiceInstance();
		const RESULT = DB.cartPointsRemoveAllItems(email, password);
	
		RESULT.then(data => 
		{ 
			write_log("\n" + "route(/cartPointsRemoveAllItems) \t RESULTS:");
			response.json({ data: data });
		}) 
		.catch(err => write_log(err));
	})
	.catch(() => 
	{
		// user is not logged in
		write_log("route(/cartPointsRemoveAllItems) \tRESULT.catch()");
		write_log("route(/cartPointsRemoveAllItems) \tif loggedIn == false");
		response.redirect('/login');
	});
});

app.patch('/update_date_of_last_visit', (request, response) =>
{
	write_log("\n"+ "route(/update_date_of_last_visit) ");
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
		write_log("update_date_of_last_visit(/) \tRESULT.then()");

		const { new_DLOV } = request.body;
		const DB = DBService.getDbServiceInstance();
		const RESULT = DB.update_date_of_last_visit(account_attributes.id, new_DLOV);
		
		console.table(request.body);
		RESULT.then((data) => 
		{
			write_log("\n" + "route(/update_date_of_last_visit) \t new_DLOV Update Success:");
			response.json({ data: data });
		}).catch(err => write_log(err));
	})
	.catch(() => 
	{ 
		write_log("route(/update_date_of_last_visit) \tRESULT.catch()");
		write_log("route(/update_date_of_last_visit) \tif loggedIn == false");
		response.redirect('/login');
	});
});


app.patch('/adminUpdateOrderStatus', (request, response) =>
{
	write_log("\n"+ "route(/adminUpdateOrderStatus) ");

	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
		write_log("adminUpdateOrderStatus(/) \tRESULT.then()");
		if (account_attributes.isAdmin == 1) 
		{
			const { orderId, status_id, status, user_id } = request.body;
			const DB = DBService.getDbServiceInstance();
			const RESULT = DB.adminUpdateOrderStatus(orderId, status_id, status, user_id);
		 
			console.table(request.body);

			RESULT.then((data) => 
			{
				write_log("\n" + "route(/adminUpdateOrderStatus) \t RESULTS:");
				response.json({ data: data });
			}).catch(err => write_log(err));
		} 
		else 
		{ 
			response.end();
		} 
	})
	.catch(() => 
	{ 
		write_log("route(/adminUpdateOrderStatus) \tRESULT.catch()");
		write_log("route(/adminUpdateOrderStatus) \tif loggedIn == false");
		response.redirect('/login');
	});
});

app.patch('/admin_update_order_tracking', (request, response) =>
    {
        write_log("\n"+ "route(/admin_update_order_tracking) ");
    
        var loggedInResponse = checkIfLoggedIn(request);
        loggedInResponse.then((account_attributes) => 
        {
            write_log("admin_update_order_tracking(/) \tRESULT.then()");
            if (account_attributes.isAdmin == 1) 
            {
                const { string_order_id, string_tracking_number, int_user_id } = request.body;
                const DB = DBService.getDbServiceInstance();
                const RESULT = DB.admin_update_order_tracking(string_order_id, string_tracking_number, int_user_id);
             
                console.table(request.body);
                console.log(string_order_id);
                console.log(string_tracking_number);
                console.log(int_user_id);

                RESULT.then((data) => 
                {
                    write_log("\n" + "route(/admin_update_order_tracking) \t RESULTS:");
                    response.json(true);
                }).catch(err => write_log(err));
            } 
            else 
            { 
                response.end();
            } 
        })
        .catch(() => 
        { 
            write_log("route(/admin_update_order_tracking) \tRESULT.catch()");
            write_log("route(/admin_update_order_tracking) \tif loggedIn == false");
            response.redirect('/login');
        });
    });

// modify
app.patch('/admin_set_search_limit', (request, response) =>
{
	// write_log("\n"+ "route(/admin_set_search_limit) ");
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
		// write_log("admin_set_search_limit(/) \tRESULT.then()");
		if (account_attributes.isAdmin == 1) 
		{ 
			const { search_limit } = request.body;
			const DB = DBService.getDbServiceInstance();
			const RESULT = DB.admin_set_search_limit(search_limit);
		 
			RESULT.then((data) => 
			{ 
				write_log("\n" + "route(/admin_set_search_limit) Update Success:");
				console.table(request.body);
				response.json(data);
			}).catch(err => write_log(err));
		} 
		else 
		{ 
			response.end();
		} 
	})
	.catch(() => 
	{ 
		write_log("route(/admin_set_search_limit) \tif loggedIn == false");
		response.redirect('/login');
	});
});

// modify
app.patch('/admin_set_menu', (request, response) =>
{
	write_log("\n"+ "route(/admin_set_menu) ");
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
		write_log("admin_set_menu(/) \tRESULT.then()");
		if (account_attributes.isAdmin == 1)
		{ 
			const { new_menu, date_menu_updated } = request.body;
			const DB = DBService.getDbServiceInstance();
			const RESULT = DB.admin_set_menu(new_menu, date_menu_updated);
		 
			RESULT.then((data) =>
			{
				write_log("\n" + "route(/admin_set_menu) Update Success:");
				console.table(request.body);
				response.json(true);
			}).catch((err) => 
			{
				response.json(false);
				write_log(err)
			});
		}
		else
		{
			response.end();
		} 
	})
	.catch(() => 
	{ 
		write_log("route(/admin_set_menu) \tif loggedIn == false");
		response.redirect('/login');
	});
});

// modify
app.patch('/admin_set_shirt_table', (request, response) =>
{
	write_log("\n"+ "route(/admin_set_shirt_table) ");
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
		write_log("admin_set_shirt_table(/) \tRESULT.then()");
		if (account_attributes.isAdmin == 1)
		{ 
			// const { string_table_name, date_menu_updated } = request.body;
            const { string_table_name, values } = request.body;

			const DB = DBService.getDbServiceInstance();
			const RESULT = DB.admin_set_shirt_table(string_table_name, values);
		 
			RESULT.then((data) =>
			{
				write_log("\n" + "route(/admin_set_shirt_table) Update Success:");
				console.table(request.body);
				response.json(true);
			}).catch((err) => 
			{
				response.json(false);
				write_log(err)
			});
		}
		else
		{
			response.end();
		} 
	})
	.catch(() => 
	{ 
		write_log("route(/admin_set_shirt_table) \tif loggedIn == false");
		response.redirect('/login');
	});
});

// create
app.post('/create_checkout_session', async (request, response) =>
{
    write_log("\n" + "route(/create_checkout_session)");

    var loggedInResponse = checkIfLoggedIn(request);
    loggedInResponse.then(async (account_attributes) => 
    {
        const DB = DBService.getDbServiceInstance();
        console.log(account_attributes.cart);
        const RESULT = DB.user_cart_calculate_total(account_attributes.cart);
        RESULT.then(async (results) =>
        { 
            // let cart_total        = results[0];
            let menuItems         = results[1];
            let stripe_line_items = [];

            for (let int_index_1 = 0; int_index_1 < account_attributes.cart.length; int_index_1++)
            {
                let element                        = account_attributes.cart[int_index_1];      // [itemId, itemQty]
                let int_element_user_cart_id       = element[0];     // [0] = itemId
                let int_element_user_cart_quantity = element[1];    // [1] = itemQty


                Array.from(menuItems).forEach(( { id, name, price } ) =>
                {
                    if ( int_element_user_cart_id == id )
                    {
                        let object_line_item =
                        {
                            price_data: 
                            {
                                currency: 'usd',
                                product_data:
                                {
                                    name: name,
                                    images: ['https://imgs.michaels.com/MAM/assets/1/726D45CA1C364650A39CD1B336F03305/img/E6F09D1C8ACB448C8AC3C230D95F63C5/10532473_r.jpg']
                                },
                                unit_amount: price * 100
                            },
                            quantity: int_element_user_cart_quantity,
                        };
                        // console.log(object_line_item);
                        stripe_line_items.push(object_line_item);
                    }
                });
            }
            // console.log('stripe_line_items');
            // console.log(stripe_line_items);
            // for each menu item
            const session = await STRIPE.checkout.sessions.create(
            {
                // line_items:
                // [
                //     {
                //         price_data: 
                //         {
                //             currency: 'usd',
                //             product_data:
                //             {
                //                 name: 'Loofy',
                //                 images: ['https://imgs.michaels.com/MAM/assets/1/726D45CA1C364650A39CD1B336F03305/img/E6F09D1C8ACB448C8AC3C230D95F63C5/10532473_r.jpg']
                //             },
                //             unit_amount: 20 * 100
                //         },
                //         quantity: 1,
                //     }, 
                //     {
                //         price_data: 
                //         {
                //             currency: 'usd',
                //             product_data:
                //             {
                //                 name: 'Long Sleeve',
                //                 images: ['https://m.media-amazon.com/images/I/41uF42-1WwL._AC_UY1000_.jpg']
                //             },
                //             unit_amount: 25 * 100
                //         },
                //         quantity: 4,
                //     },
                // ],
                line_items: stripe_line_items,
                mode: 'payment',
                success_url: `http://localhost:8040/user_place_order_stripe`,
                cancel_url:  `http://localhost:8040/checkout`,
                automatic_tax: {enabled: false},
            });
            response.json(session.url);
        })
        .catch((error) => 
        {
            write_log('route(/user_cart_calculate_total) ERROR:');
            write_log(error);
            console.trace(error);
            response.json(error);
        });
    })
    .catch(() =>
    {
        write_log("route(/create_checkout_session) \tif loggedIn == false");
        response.redirect('/login');
    });
});

// TODO, delete this? Not needed?
app.get('/user_cart_calculate_total', (request, response) =>  
{
    write_log("\n" + "route(/user_cart_calculate_total) ");

    var loggedInResponse = checkIfLoggedIn(request);
    loggedInResponse.then((account_attributes) => 
    {
        const DB = DBService.getDbServiceInstance();
        const RESULT = DB.user_cart_calculate_total(account_attributes.cart);

        RESULT.then(async (data) =>
        { 
            write_log("\n" + "RESULT(/user_cart_calculate_total) Success");
            response.json(data);
        })
        .catch((error) => 
        {
            write_log('route(/user_cart_calculate_total) ERROR:');
            write_log(error);
            response.json(error);
        });
    })
    .catch(() =>
    {
        write_log("route(/user_cart_calculate_total) \tif loggedIn == false");
        response.redirect('/login');
    });
});

// create 
app.post('/user_place_order_cash', (request, response) =>  
{
	write_log("\n" + "route(/user_place_order_cash) ");

	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{
        const STRING_PAYMENT_METHOD = 'Cash';
		const DB = DBService.getDbServiceInstance();
		const RESULT = DB.user_place_order(account_attributes, new Date(), STRING_PAYMENT_METHOD);

		RESULT.then(async (data) =>
		{ 
			write_log("\n" + "RESULT(/user_place_order_cash) Success");
			response.json(true);
		})
		.catch((error) => 
		{
			write_log('route(/user_place_order_cash) ERROR:');
			write_log(error)
			response.json(error);
		});
	})
	.catch(() =>
	{
		write_log("route(/user_place_order_cash) \tif loggedIn == false");
		response.redirect('/login');
	});
});

// should be POST instead of GET, but cant because of /create_checkout_session redirect
app.get('/user_place_order_stripe', (request, response) =>  
{
    write_log("\n" + "route(/user_place_order_stripe) ");

    var loggedInResponse = checkIfLoggedIn(request);
    loggedInResponse.then((account_attributes) => 
    {
        const STRING_PAYMENT_METHOD = 'Stipe';
        const DB = DBService.getDbServiceInstance();
        const RESULT = DB.user_place_order(account_attributes, new Date(), STRING_PAYMENT_METHOD);

        RESULT.then(async (data) =>
        { 
            write_log("\n" + "RESULT(/user_place_order_stripe) Success");
            response.redirect('/orders');
        })
        .catch((error) => 
        {
            write_log('route(/user_place_order_stripe) ERROR:');
            write_log(error);
            response.json(error);
        });
    })
    .catch(() =>
    {
        write_log("route(/user_place_order_stripe) \tif loggedIn == false");
        response.redirect('/login');
    });
});

// create 
app.post('/register', (request, response) =>  
{ 
	write_log("\n" + ".(/register) POST");
	const { name, email, password, code} = request.body;
	write_log(request.body);
	const DB = DBService.getDbServiceInstance();
	const RESULT = DB.createUserAccount(name, password, email, code);
 
	RESULT 
	.then((RESULT) =>  
	{ 
		write_log("\n" + ".then(/register) POST");

		var options = 
		{ 
			maxAge: 1000 * 60 * 20160, // Would expire after two weeks (20160 minutes)
			httpOnly: false, // The cookie only accessible by the web server 
			signed: false // Indicates if the cookie should be signed 
		} 

		// Set cookie 
		response.cookie('email', email, options) // options is optional 
		response.cookie('password', password, options) // options is optional 
		write_log("Cookies created: name, password");
		response.json(true);
	}) 
	.catch(err => // Error: Account could not be created 
	{ 
		write_log("/createUserAccount " + "\n" + "Error: Account could not be created");
		write_log(err) 
		response.json(false);
	});
});

app.post('/customerSupportEmailHelpDesk', (request, response) =>
{
	write_log("\n" + "route(/customerSupportEmailHelpDesk) ");

	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{ 
		write_log("customerSupportEmailHelpDesk(/) \tRESULT.then()");

		var { order_id, description } = request.body;

		const DB = DBService.getDbServiceInstance();
		const RESULT = DB.customerSupportEmailHelpDesk(account_attributes, order_id, description);
		
		RESULT.then((RESULT) => 
		{
			write_log("\n" + "route(/customerSupportEmailHelpDesk) \t RESULTS: Success");
			response.json(true);
		})
		.catch((err) => 
		{
			write_log("\n" + "route(/customerSupportEmailHelpDesk) \t CATCH:");
			write_log(err);
			response.json(false);
		});
	})
	.catch(() => 
	{
		// user is not logged in
		write_log("route(/customerSupportEmailHelpDesk) \tRESULT.catch()");
		response.redirect('/login');
	});
});

app.post('/customerSupportEmailFeedback', (request, response) =>
{
	write_log("\n" + "route(/customerSupportEmailFeedback) ");

	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{ 
		write_log("customerSupportEmailFeedback(/) \tRESULT.then()");

		var { subject, description } = request.body;
		var userEmail = request.cookies.email;

		const DB = DBService.getDbServiceInstance();
		const RESULT = DB.customerSupportEmailFeedback(account_attributes, subject, description);
		
		RESULT.then((RESULT) => 
		{
			write_log("\n" + "route(/customerSupportEmailFeedback) \t RESULTS: Success");
			response.json(true);
		})
		.catch((err) => 
		{
			write_log("\n" + "route(/customerSupportEmailFeedback) \t CATCH:");
			write_log(err);
			response.json(false);
		});
	})
	.catch(() => 
	{
		// user is not logged in
		write_log("route(/customerSupportEmailFeedback) \tRESULT.catch()");
		response.redirect('/login');
	});
});

app.post('/email_error', (request, response) =>
{
	write_log("\n" + "route(/email_error) ");

	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{ 
		write_log("email_error(/) \tRESULT.then()");
		var {error_name, error_message, error_lineNumber} = request.body;

		const DB = DBService.getDbServiceInstance();
		// const RESULT = DB.email_error(account_attributes, error_name, error_message, error_lineNumber);
		
		// RESULT.then((data) => 
		// {
		// 	write_log("\n" + "route(/email_error) \t RESULTS: Success");
		// 	response.json(true);
		// })
		// .catch((err) => 
		// {
		// 	write_log("\n" + "route(/email_error) \t CATCH:");
		// 	write_log(err);
		// 	response.json(false);
		// });
	})
	.catch(() => 
	{
		// user is not logged in
		write_log("route(/email_error) \tRESULT.catch()");
		response.redirect('/login');
	});
});

// delete 
app.delete('/cancelOrder', (request, response) => 
{
	var loggedInResponse = checkIfLoggedIn(request);
	loggedInResponse.then((account_attributes) => 
	{ 
		// write_log("\n"+ "route(/cartAddItem) ");
		const email = request.cookies.email;
		const password = request.cookies.password;
		const { order_id } = request.body;
		const DB = DBService.getDbServiceInstance();
		const RESULT = DB.cancelOrder(order_id, email, password);
	
		RESULT.then(data => 
		{ 
			write_log("\n" + "route(/cancelOrder) \t RESULTS:");
			response.json({ data: data });
		}) 
		.catch(err => write_log(err));
	})
	.catch(() => 
	{
		// user is not logged in
		write_log("route(/cancelOrder) \tRESULT.catch()");
		response.redirect('/login');
	});
});
 
// check if user is logged in  
function checkIfLoggedIn(request) 
{ 
	const response = new Promise((resolve, reject) => 
	{ 
		const email = request.cookies.email;
		const password = request.cookies.password;
 
		// write_log("email, password: ");
		// write_log(email);
		// write_log(password);
 
		// no login creds 
		if (typeof email == 'undefined' || typeof password == 'undefined') 
		{ 
			write_log("loggedIn == false");
			reject(false);
			return;
		} 
 
		const DB = DBService.getDbServiceInstance();
		const RESULT = DB.getUserData(email, password);
 
		RESULT.then((RESULTs) => // valid login 
		{ 
			// write_log("loggedIn == true");
			// write_log(RESULTs);
			resolve(RESULTs);
		})
		.catch(() =>  // invalid login 
		{ 
			write_log("loggedIn == false");
			reject(false);
		});
	});
	return response;
}

const STRING_LOGS_FOLDER_NAME = `logs/${new Date().toISOString().split('T')[0]}`;

// Create Folders
FS.mkdirSync(STRING_LOGS_FOLDER_NAME, { recursive: true });

let ARRAY_LOGS_FILES      = FS.readdirSync(STRING_LOGS_FOLDER_NAME);
let STRING_LOGS_FILE_NAME = `${ARRAY_LOGS_FILES.length}_debug`;

// files saved to these directories
const STRING_DIR_LOGS = `${STRING_LOGS_FOLDER_NAME}/${STRING_LOGS_FILE_NAME}.log`;

function write_log(text)
{
    let date_now = new Date();
    FS.appendFileSync(STRING_DIR_LOGS, `\n${date_now.toISOString().split('T')[0].replace(/-/g, '/') + ', ' + date_now.toLocaleTimeString() + ': \t' + text}`, 'utf-8');
}
 
// app.listen( process.env.PORT, '174.170.105.180', () =>
    app.listen( process.env.PORT, () =>
    {
        console.log('Listening on PORT: ' + process.env.PORT);
        write_log('Listening on PORT: ' + process.env.PORT);
    });
    
// server listening to PORT 
// app.listen(process.env.PORT);


