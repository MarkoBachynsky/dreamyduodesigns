console.log('==================================================\n\n\n\n');

// start sale
var start_date = new Date('2022-08-24T16:00:00.000');

// order placed
var todayDate = new Date();

// end sale
var end_date = new Date('2022-08-26T16:06:00.000');


var Difference_In_Time1 = start_date.getTime() - todayDate.getTime();
var Difference_In_Time2 = end_date.getTime() - todayDate.getTime();


console.table([['Difference_In_Time1', Difference_In_Time1], ['Difference_In_Time2', Difference_In_Time2]]);


// case 1 today date is before start date 
if (Difference_In_Time1 > 0)
{
    console.log('case 1');
}
// case 2 today date is after start date && today date is before end date
else if ( Difference_In_Time1 <= 0 && Difference_In_Time2 >= 0 )
{
    console.log('case 2');
}
// case 3 today date is after start date && today date is after end date
else if ( Difference_In_Time1 <= 0 && Difference_In_Time2 <= 0 )
{
    console.log('case 3');
}


console.log('\n\n\n==================================================\n\n\n\n');
