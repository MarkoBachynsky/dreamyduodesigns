const CryptoJS = require("crypto-js");
const dotenv = require('dotenv');
const mysql = require('mysql2');
const nodemailer = require('nodemailer');
const { resolve } = require("path");


var instance = null;
dotenv.config();

const connection = mysql.createPool(
    {
        connectionLimit : 100, // <- Keep an eye on this
        host: process.env.HOST,
        user: process.env.USER,
        password: process.env.PASSWORD,
        database: process.env.DATABASE,
        multipleStatements: true,
        port: process.env.DB_PORT
    });

connection.on('acquire', ( connection ) =>
    {
        // console.log('Connection %d acquired', connection.threadId);
    });

connection.on('connection', ( connection ) =>
    {
        // connection.query('SET SESSION auto_increment_increment=1')
        // console.log('Connection %d event', connection.threadId);
    });

connection.on('enqueue', () =>
    {
        // console.log('connection enqueue');
        // console.log('Waiting for available connection slot.');
    });

connection.on('release', ( connection ) =>
    {
        // console.log('Connection %d released', connection.threadId);
    });

console.log("\n" + "Server Started:");
console.log(process.env.HOST);
console.log(process.env.USER);
console.log(process.env.PASSWORD);
console.log(process.env.DATABASE);
console.log(process.env.TABLE_ITEMS);
console.log(process.env.TABLE_NAMES);
console.log(process.env.TABLE_ORDERS);
console.log(process.env.PORT);
console.log(process.env.DB_PORT);
console.log("\n");

var address = 'https://dreamyduodesigns.com';

class DbService
{
    static getDbServiceInstance()
    {
        return instance ? instance : new DbService();
    }

    async getAllData()
    {
        const response = await new Promise((resolve, reject) => 
        {
            try
            {
            const query1 = "SELECT * FROM " + process.env.TABLE_NAMES + ";";
            const query2 = "SELECT * FROM " + process.env.TABLE_ORDERS + ";";
            connection.query(query1 + query2, [1, 2], (err, results) =>
            {
                if (err) reject(new Error("getAllData ERROR\n" + err.message));
                // console.log(results[0]); // [{1: 1}]
                // console.log(results[1]); // [{2: 2}]
                // console.log("\n");
                resolve(results);
            });

            } catch (error)
            {
                console.log(error);
                reject();
            }
        });
        return response;
    }

    async getUserData(email, password)
    {
        const response = await new Promise((resolve, reject) => 
        {
            try
            {
                email = email.toLowerCase();
                const query = "SELECT * FROM " + process.env.TABLE_NAMES + " WHERE email = ?;";
                connection.query(query, [email, password], (err, results) =>
                {
                    if (err) 
                    {
                        reject(new Error("DBService.js getUserData(email, password) ERROR\n" + err.message));
                    }
                    else
                    {
                        try
                        {
                            if (results.length > 0)
                            {
                                let decryptedText = CryptoJS.AES.decrypt(results[0].password, process.env.KEY).toString(CryptoJS.enc.Utf8);
        
                                console.log('OriginalText: ' + password);
                                console.log('decryptedText: ' + decryptedText);
        
                                // if (password == decryptedText)
                                if (password == results[0].password)
                                {
                                    // console.log('Login success!');
                                    resolve(results[0]);
                                }
                                else
                                {
                                    reject('Wrong password');
                                }
                            }
                            else
                            {
                                reject('Wrong email.');
                            }
                        } 
                        catch (error)
                        {
                            reject('/getUserData(email, password) - DATABASE ERROR');
                        }
                    }
                });
            }
            catch (error)
            {
                console.log(error);
                reject(error);
            }
            });
        return response;
    }

    async get_menu()
    {
        const response = await new Promise((resolve, reject) => 
        {
            try
            {
                const sql = "SELECT * FROM " + process.env.TABLE_ITEMS + ";";
                connection.query(sql, (err, results) =>
                {
                    if (err) 
                    {
                        reject(new Error("DBService.js ERROR\n" + err.message));
                    }
                    else
                    {
                    // console.log("\n" + "get_menu()  Results:");
                    // console.log(results);
                    resolve(results);
                    }
                });
            } catch (error)
            {
                console.log(error);
                reject(error);
            }
        });
        return response;
    }

    async get_table_shirts()
    {
        const response = await new Promise((resolve, reject) => 
        {
            try
            {
                const sql = "SELECT * FROM " + process.env.TABLE_SHIRTS + ";";
                connection.query(sql, (err, results) =>
                {
                    if (err) 
                    {
                        reject(new Error("DBService.js ERROR\n" + err.message));
                    }
                    else
                    {
                    // console.log("\n" + "get_table_shirts()  Results:");
                    // console.log(results);
                    resolve(results);
                    }
                });
            } catch (error)
            {
                console.log(error);
                reject(error);
            }
        });
        return response;
    }

    async setCartData(account_attributes, cart)
    {
        const response = await new Promise((resolve, reject) =>
        {
            try
            {
                const query = "UPDATE " + process.env.TABLE_NAMES + " SET cart = ? WHERE id = ?;";
                connection.query(query, [JSON.stringify(cart), account_attributes.id], (err, results) =>
                {
                    if (err)
                    {
                        reject("DBService.js getUserData(email, password) ERROR\n" + err.message);
                    }
                    else
                    {
                        results.affectedRows;    // Save SQL changes
                        resolve(cart);
                    }
                });
            } 
            catch (error)
            {
                console.log(error);
                reject(false);
            }
        });
        return response;
    }

    async setCartPointsData(email, password, cart)
    {
        const response = await new Promise((resolve, reject) =>
        {
            try
            {
                const query = "UPDATE " + process.env.TABLE_NAMES + " SET cart_points = ? WHERE email = ?;";
                connection.query(query, [cart, email], (err, results) =>
                {
                    if (err) 
                    {
                        reject(new Error("DBService.js setCartPointsData(email, password) ERROR\n" + err.message));
                    }

                    if (results.length > 0)
                    {
                        let decryptedText = CryptoJS.AES.decrypt(results[0].password, process.env.KEY).toString(CryptoJS.enc.Utf8);

                        // console.log('OriginalText: ' + password);
                        // console.log('decryptedText: ' + decryptedText);

                        if (password == decryptedText)
                        {
                            // console.log('Login success!');
                            resolve(results);
                            return;
                        }
                    }
                    results.affectedRows;     // Save SQL changes
                    cart = JSON.parse(cart); // Return user's cart as JSON object
                    resolve(cart);
                    return;
                });
            } catch (error)
            {
                console.log(error);
                reject(false);
            }
        });
        return response;
    }

    async setCartPointsDataRemove(email, password)
    {
        const response = await new Promise((resolve, reject) =>
        {
            try
            {
                const query = "UPDATE " + process.env.TABLE_NAMES + " SET cart_points = ? WHERE email = ?;";
                connection.query(query, ['', email], (err, results) =>
                {
                    if (err) 
                    {
                        reject(new Error("DBService.js setCartPointsDataRemove(email, password) ERROR\n" + err.message));
                    }

                    if (results.length > 0)
                    {
                        let decryptedText = CryptoJS.AES.decrypt(results[0].password, process.env.KEY).toString(CryptoJS.enc.Utf8);

                        // console.log('OriginalText: ' + password);
                        // console.log('decryptedText: ' + decryptedText);

                        if (password == decryptedText)
                        {
                            // console.log('Login success!');
                            resolve(true);
                            return;
                        }
                    }
                    results.affectedRows;     // Save SQL changes
                    resolve(true);
                    return;
                });
            } catch (error)
            {
                console.log(error);
                reject(false);
            }
        });
        return response;
    }

    // Return's user's cart after action
    async cartAddItem(account_attributes, itemId, itemQty)
    {
        const response = await new Promise((resolve, reject) => 
        {
            const DB = DbService.getDbServiceInstance();
            // Grab user's cart from database with given email and password
            if (itemId == null)
            {
                reject('ItemID is NULL');
            }
            let cart = [[0, 0]];
            try
            {
                // retrieve the user's cart
                cart = account_attributes.cart;
            }
            catch (error)
            {
                console.log(`cartAddItem(${account_attributes}, ${itemId}, ${itemQty})`);
                console.log('cart = results[0].cart; FAILURE');
                console.log(error);

                // if user's cart is null, make it an empty array
                cart = [[0, 0]];
            }

            /*  loop through user's cart
                if item exists, add to its quantity
                if item does not exist, add the item's id and its quantity
            */
            let itemIdExists = false;
            for (let index = cart.length - 1; index >= 0; index--)
            {
                let element = cart[index];    // [itemId, itemQty]
                let elementId = element[0];     // [0] = itemId
                let elementQty = element[1];     // [1] = itemQty

                // if item exists in user's cart, add to its quantity
                if (elementId == itemId)
                {
                    element[1] += itemQty;
                    itemIdExists = true;
                }
            }
            // console.log('helloooo')
            // if item does not exist in user's cart, add the item's id and its quantity
            if (itemIdExists == false)
            {
                cart.push([itemId, itemQty]);
                // sort ascending by itemId[0] [itemId, itemQty]
                cart.sort(( a, b ) => a[ 0 ] - b[ 0 ]);
            }
            // console.log('helloooo2')

            // Add qty to size [[0, (size)], [1,5]]
            cart[0][1] += itemQty;
            // console.log('helloooo3')

            // turn user's cart into JSON object for storage into database

            // store user's cart JSON object into the database
            let setCart = DB.setCartData(account_attributes, cart);
            setCart.then((cartResult) => 
            {
                // user's cart is now saved
                console.log("CartResult: ");
                console.log(cartResult);
                resolve(cartResult);
            });
        });
        return response;
    }

    // Return's user's cart after action
    async cartSubtractItem(account_attributes, itemId, itemQty)
    {
        const response = await new Promise((resolve, reject) => 
        {
            const DB = DbService.getDbServiceInstance();
            // Grab user's cart from database with given email and password

            // console.log("cartSubtractItem .then");
            let cart = [[0, 0]];
            try
            {
                // retrieve the user's cart
                cart = account_attributes.cart;
                if (cart.length < 1)
                {
                    return;
                }
            }
            catch (error)
            {
                // if user's cart is null, exit function
                return;
            }

            /*  loop through user's cart
                if item exists, remove to its quantity
            */
            let targetIndex = -1;

            // for loop, decending index - not necessary though
            for (let index = cart.length - 1; index >= 0; index--)
            {
                let element = cart[index];
                let elementId = element[0];
                let elementQty = element[1];

                if (elementId == null)
                {

                }
                if (elementId == itemId)
                {
                    // item found, remove QTY
                    if (elementQty > itemQty)
                    {
                        element[1] -= itemQty;
                    }
                    else
                    {
                        targetIndex = index;
                    }
                    cart[0][1] -= itemQty;
                }
            }

            if (targetIndex > -1)
            {
                cart.splice(targetIndex, 1);
            }

            // store user's cart JSON object into the database
            let setCart = DB.setCartData(account_attributes, cart);
            setCart.then((cartResult) => 
            {
                // user's cart is now saved
                // console.log("CartResult: ");
                // console.log(cartResult);
                resolve(cartResult);
            });
        });
        return response;
    }

    async cartRemoveAllItems(account_attributes)
    {
        const DB = DbService.getDbServiceInstance();
        const response = await new Promise((resolve, reject) => 
        {
            // store user's cart JSON object into the database
            let setCart = DB.setCartData(account_attributes, [[0, 0]]);
            setCart.then(() => 
            {
                // user's cart is now saved
                resolve(setCart);
            });
        });
        return response;
    }

    async cartPointsRemoveAllItems(email, password)
    {
        const DB = DbService.getDbServiceInstance();
        const response = await new Promise((resolve, reject) => 
        {
            // store user's cart JSON object into the database
            let setCart = DB.setCartPointsData(email, password, cart);
            setCart.then(() => 
            {
                // user's cart is now saved
                resolve(setCart);
            });
        });
        return response;
    }

    // returns [cartTotal, menuItems]
    // cartItems = cart except id is replaced by names [[name, qty]]
    // used for users submitted order (SHIRTS)
    async user_cart_calculate_total(cart)
    {
        const response = await new Promise((resolve, reject) => 
        {
            try
            {
                const DB = DbService.getDbServiceInstance();
                let cartTotal = 0;
                let menuData = DB.get_table_shirts();
                let array_selected_menu_items = [];
                menuData.then(data => 
                {
                    // console.log(data);
                    let menuItems = Array.from(data);
                    /*  loop through user's cart
                        Add each item's quantity * cost 
                    */
                    for (let index = 0; index < cart.length; index++)
                    {
                        let element = cart[index];      // [itemId, itemQty]
                        let elementId = element[0];     // [0] = itemId
                        let elementQty = element[1];    // [1] = itemQty
    
                        // for each menu item
                        Array.from(menuItems).forEach(( { id, name, price, discount, product, style, color } ) =>
                        {
                            if ( elementId == id )
                            {
                                // Add name to cartItems
                                cart[ index ].push( name );
                                // cartTotal += userItemQty * itemPrice
                                cartTotal += ( element[ 1 ] * price );
                                let description = `(${elementQty}) ${name} ${product} ${style} ${color}`;
                                array_selected_menu_items.push({ id, name, price, discount, description})
                            }
                        });
                    }
                    // console.log("\nCalculated total:");
                    // console.log(cartTotal);

                    // console.log("Cart items:");
                    // console.log(cartItems);
                    resolve([cartTotal, array_selected_menu_items]);
                });
            }
            catch (error)
            {
                console.log(error);
                reject(error);
            }
        });
        return response;
    }

    async consumeAccountCreationCode(code)
    {
        const response = await new Promise((resolve, reject) => 
        {
            console.log(`consumeAccountCreationCode(${code})`);
            const query = "DELETE FROM " + process.env.TABLE_CODES + " WHERE (code = ?);";
            connection.query(query, [code], (err, result) =>
            {
                if (err)
                {
                    reject(err);
                }
                else
                {
                    if (result.affectedRows == 0)
                    {
                        reject('No code consumed');
                    }
                    else
                    {
                        console.log(`Code: ${code} CONSUMED`);
                        resolve(result.affectedRows);
                    }
                }
            });
        });
        return response;
    }

    async generateAccessCodes(quantity)
    {
        const response = await new Promise((resolve, reject) => 
        {
            console.log(`/generateAccessCodes(${quantity})`);
            let values = [];
            for (let i = 0; i < quantity; i++)
            {
                let code  = Math.random().toString(36).toUpperCase().substr(2,8); // Random 8 char alphanumeric - uppercase only
                console.log(code);
                code = [code];
                values.push(code);
            }
            // values = [values];
            // console.log("values");
            // console.log(values);
            const sql = "INSERT INTO " + process.env.TABLE_CODES + " (code) VALUES ?;";
            connection.query(sql, [values], (err, result) =>
            {
                if (err) 
                {
                    reject(err);
                }
                else
                {    

                    resolve(result.insertId);
                }
            });
        });
        return response;
    }

    async generateAccountCreationSingleCode(code)
    {
        code = code.toUpperCase();

        const response = await new Promise((resolve, reject) => 
        {
            console.log(`/generateAccountCreationSingleCode(${code})`);

            const sql = "INSERT INTO " + process.env.TABLE_CODES + " (code) VALUES (?);";
            let values = [];
            values.push(code);
            connection.query(sql, [values], (err, result) =>
            {
                if (err) 
                {
                    reject(err);
                }
                else
                {    
                    resolve(result.insertId);
                }
            });
        });
        return response;
    }

    // create user order
    async user_place_order(account_attributes, date_created, payment_method)
    {
        console.log('DB.service /user_place_order()');

        const response = await new Promise((resolve, reject) => 
        {
            try
            {
                const DB = DbService.getDbServiceInstance();
                let menuItems, cart_total = null;
                let date_obj_date_created = new Date(date_created);

                // create unique order ID
                const order_id  = Math.random().toString(36).toUpperCase().substr(2,6); // Random 6 char alphanumeric - uppercase only

                // get cart total cost, and menu items with names
                DB.user_cart_calculate_total(account_attributes.cart)
                .then((results) =>
                {
                    console.log('results');
                    console.log(results);

                    cart_total   = results[0];
                    menuItems    = results[1];

                    // if cart total is zero, reject
                    if (cart_total <= 0)
                    {
                        reject('Cart Total is ' + 0);
                        return;
                    }

                    // Add full description of each selected menu item into (1) string for the order description
                    let string_description = '';
                    Array.from(menuItems).forEach(( { description } ) =>
                    {
                        string_description += `${description}+`;
                    });
                    // Remove extra '+'
                    string_description = string_description.slice(0, -1);
                    
                    const status = "Payment Required";

                    let sql_statement = "INSERT INTO " + process.env.TABLE_ORDERS + " (user_id, status_id, status, order_id, name, email, cart, total, date_created, payment_method, description) VALUES (?);";
                    let values = [account_attributes.id, 1, status, order_id, account_attributes.name, account_attributes.email, JSON.stringify(account_attributes.cart), cart_total, date_created, payment_method, string_description];

                    console.log(values);

                    connection.query(sql_statement, [values], (error1, result1) =>
                    {
                        if (error1)
                        {
                            reject(error1.message);
                        }
                        else
                        {
                            // clear user cart
                            DB.cartRemoveAllItems(account_attributes);

                            // set user date_orderLastPlaced to now
                            const query2 = "UPDATE " + process.env.TABLE_NAMES + " SET date_lastOrderPlaced = ? WHERE id = ?;";
                            connection.query(query2, [date_created, account_attributes.id], (error2, result2) =>
                            {
                                if (error2) 
                                {
                                    reject(error2);
                                }
                                else
                                {

                                    // order created, now email user
                                    // Email text
                                    let cartText = "";
                                    for (let j = 1; j < account_attributes.cart.length; j++)
                                    {
                                        // for each menu item
                                        Array.from(menuItems).forEach(( { id, name, price } ) =>
                                        {
                                            let elementID = account_attributes.cart[ j ][ 0 ];
                                            if ( elementID == id )
                                            {
                                                let cartElement = account_attributes.cart[ j ];
                                                let itemQty = cartElement[ 1 ];
                                                cartText += `<b>[${ itemQty }]</b> ${ name } ($${ price } per) <br>`;
                                            }
                                        });
                                    }

                                    let subject = `Strap in ${account_attributes.name}. We're blasting off! 🚀 (${order_id})`;
                                    let html = 
                                    `
                                    <h3>Your order has been submitted!</h3>
                                    <p>
                                    Send your payment to complete your purchase.<br>
                                    Include your order ID.<br>
                                    </p>
                                    <h3>Order Information</h3>
                                    <p>
                                    Status:  <a href="https://account.venmo.com/pay?txn=pay&recipients=Astro-Medibles">Payment Required</a><br>
                                    Order ID:  ${order_id}<br>
                                    Items:<br>
                                    ${cartText}
                                    <br>
                                    Payment Method: ${payment_method}<br>
                                    Total:  $${cart_total.toFixed(2)}<br>
                                    <br>
                                    To cancel your order, go to <a href="${address}/orders">My Orders</a>.<br>
                                    After we confirm your payment, your order status will be updated and you will be notified.
                                    <br>
                                    This is an automated message.
                                    </p>
                                    `;     
                                    // // DB.sendEmail_old(account_attributes.email, subject, html);


                                    resolve([result1.insertId, result2.affectedRows]);
                                }
                            });
                        }
                    });
                });
            } 
            catch (error)
            {
                reject(error);
            }
        });
        return response;
    }

    async createUserAccount(name, password, email, accessCode)
    {
        const DB = DbService.getDbServiceInstance();
        const response = new Promise((resolve, reject) =>
        {
            try
            {
                // setup default account attributes
                accessCode = accessCode.toUpperCase();
                email = email.toLowerCase();
                const isAdmin = 0;
                let date_created = new Date().toISOString();
                password = CryptoJS.AES.encrypt(password, process.env.KEY).toString();
                let cart = [[0, 0]];

                const query = "DELETE FROM " + process.env.TABLE_CODES + " WHERE (code = ?);";
                connection.query(query, [accessCode], (err, result) =>
                {
                    if (err)
                    {
                        reject(err);
                    }
                    else // else, no error
                    {
                        if (result.affectedRows == 0)
                        {
                            reject('No code consumed, account NOT created.');
                        }
                        else
                        {
                            console.log(`Access Code: ${accessCode} CONSUMED`);
    
                            let query2 = "INSERT INTO " + process.env.TABLE_NAMES + " (isAdmin, name, password, cart, email, date_created, verificationCode, date_lastOrderPlaced) VALUES (?,?,?,?,?,?,?,?);";
                            connection.query(query2, [isAdmin, name, password, JSON.stringify(cart), email, date_created, accessCode, ''], (err, result) =>
                            {
                                if (err) 
                                {
                                    // If there was an error, add the unique code back to the DB
                                    DB.generateAccountCreationSingleCode(accessCode)
                                    .then(results => 
                                    {
                                        reject(err);
                                    }).catch((error) =>
                                    {
                                        console.log(error);
                                        reject(err);
                                    });
                                }
                                else
                                {
                                    let subject = "Welcome " + name + "! Your account has been created! 🚀";
                                    let html = 
                                    `
                                    <h3>Welcome to the new website!</h3>
                                    <p>
                                    Head over to <a href="${address}">dreamyduodesigns.com</a> to start your order.
                                    <br>
                                    This is an automated message.
                                    </p>
                                    `;
                                    
                                    // DB.sendEmail_old(email, subject, html);
                                    
                                    console.log('Account Created!');
                                    resolve([result.insertId, result.affectedRows]);
                                }
                            });
                        }
                    }
                });
            }
            catch (error)
            {
                reject('ERROR DB /createUserAccount() \n' + error);
            }
        });
        return response;
    }

    async get_user_orders(user_id)
    {
        const response = await new Promise((resolve, reject) => 
        {
            try
            {
                const sql = "SELECT * FROM " + process.env.TABLE_ORDERS + " WHERE user_id = ? ORDER BY date_created DESC;";
                connection.query(sql, [user_id], (error, results) =>
                {
                    if (error)
                    {
                        reject("DBService.js ERROR /get_user_orders\n" + error);
                    }
                    else if (typeof results == 'undefined') 
                    {
                        reject("/get_user_orders: Orders are undefined.");
                    }
                    else
                    {
                        resolve(results);
                    }
                });
            }
            catch (error)
            {
                reject(error);
            }
        });
        return response;
    }

    async update_date_of_last_visit(user_id, new_DLOV)
    {
        const response = new Promise((resolve, reject) =>
        {

            try
            {
                const query = "UPDATE " + process.env.TABLE_NAMES + " SET date_last_visited = ? WHERE id = ?;";
                connection.query(query, [new_DLOV, user_id], (err, result) =>
                {
                    if (err)
                    {
                        reject(err.message);
                    }
                    else
                    {
                        console.log("new_DLOV Update Success"); 
                        resolve(result.affectedRows);
                    }
                });
            } catch (error)
            {
                // Send Dev Error
                let subject = 'DB.SERVICE ERROR - /update_date_of_last_visit';
                console.log(subject);
                console.log(error);
                console.log(error.stack);

                let html    = `<p>
                ${error.message}
                <br><br>
                ${error.stack}
                </p>`;
                const DB = DbService.getDbServiceInstance();
                // DB.sendEmail_old(process.env.AM_USER_DEV01, subject, html);
            }
        });
        return response;
    }


    async adminUpdateOrderStatus(orderId, status_id, status, user_id)
    {
        const response = new Promise((resolve, reject) =>
        {
            // console.log(order_id);
            const DB = DbService.getDbServiceInstance();

            try
            {
                // if status code does not equal delete
                if (status_id != 0)
                {
                    const query = "UPDATE " + process.env.TABLE_ORDERS + " SET status_id = ?, status = ? WHERE order_id = ?;";
                    connection.query(query, [status_id, status, orderId], (err, result) =>
                    {
                        if (err)
                        {
                            reject(err.message);
                        }
                        else
                        {
                            // get user info ~ incomplete
                            const query2 = "SELECT * FROM " + process.env.TABLE_NAMES + " WHERE id = ?;";
                            connection.query(query2, [user_id], (err, result2) =>
                            {
                                if (err) 
                                {
                                    console.log('Tried to search info from user_id:' + user_id);
                                    reject(err);
                                }
                                else
                                {
                                    result2 = result2[0];
                                    console.table(result2);
        
                                    console.log(`Order ${orderId} : Statis is now ${status}!`);
                                    
                                    try
                                    {
                                        let userEmail = result2.email;


                                        // console.table(['userEmail', userEmail]);
            
                                        let subject = `Order ${orderId}, Status: ${status}`;
                                        let html = 
                                        `
                                        <h3>Your Order Status has been updated to ${status}</h3>
                                        <p>
                                        Visit <a href="https://www.dreamyduodesigns.com/orders">this link</a> to see your order status.
                                        <br>
                                        This is an automated message.
                                        </p>
                                        `;
                    
                    
                                        if (status == 'Payment Required')
                                        {
                                            // console.log('Status Case: 1');
                    
                                            
                                        } else if (status == 'Preparing Order')
                                        {
                                            // console.log('Status Case: 2');
                                            html = 
                                            `
                                            <h3>Your Order Status has been updated to ${status}</h3>
                                            <p>
                                            This means that we have confirmed your payment, and your order is being prepared.
                                            <br>
                                            You will recieve a notification once your order status has been updated to 'Ready for Pickup'.
                                            <br>
                                            When your order is Ready for Pickup, you will be able to select your scheduled pick up day & time. Along with directions and an address.
                                            <br>
                                            <br>
                                            Visit <a href="https://www.dreamyduodesigns.com/orders">www.dreamyduodesigns.com/orders</a> to see your order status.
                                            <br>
                                            <br>
                                            This is an automated message.
                                            </p>
                                            `;
                                        } else if (status == 'Ready for Pickup')
                                        {
                                            // console.log('Status Case: 3');
                    
                                            html = 
                                            `
                                            <h3>Step 1: Select Pickup Day & Time</h3>
                                            <p>
                                            First, confirm your pickup time & location at <a href="https://www.dreamyduodesigns.com/orders">www.dreamyduodesigns.com/orders</a>.
                                            <br>
                                            After you have selected your confirmed pickup time, follow these directions for pickup.
                                            </p>
                                            <br>
                                            <h3>Step 2: Pickup At Location</h3>
                                            <p>
                                            <b>Directions for Pickup 77504 (Lazy Daze)</b>
                                            <br>
                                            <a href="https://goo.gl/maps/yG57sXc9Mt3jaQMr8">4416 Fairmont Pkwy Ste 103, Pasadena, TX 77504</a>
                                            <br>
                                            Lazy Daze is avalible for pickup anytime within their business hours.
                                            <br><br>
                    
                                            <b>Directions for Pickup 77598 (Apartment)</b>
                                            <br>
                                            <a href="https://goo.gl/maps/jQcvTGmZJdWFp3q16">18833 Town Ridge Ln, Webster, TX 77598</a>
                                            <br>
                                            Do not enter the apartment complex. Please park on the side on the street, closer to Retail Rd. Park on any side of the street.
                                            <br>
                                            Let me know you have arrived.
                                            <br>
                    
                                            <br>
                                            Message me here: <a href="https://twitter.com/DreamyDuoDesigns">dreamyduodesigns Twitter</a>
                                            <br>
                                            <br>
                                            Visit <a href="https://www.dreamyduodesigns.com/orders">www.dreamyduodesigns.com/orders</a> to see your order status.
                                            <br>
                                            <br>
                                            This is an automated message.
                                            </p>
                                            `;
                    
                    
                                        } else if (status == 'Complete')
                                        {
                                            // console.log('Status Case: 4');
                                            html = 
                                            `
                                            <p>
                                            We confirmed you have picked up your order, and we hope you enjoy!
                                            <br>
                                            <b>Thank you for choosing dreamyduodesigns!</b>
                                            <br>
                                            <br>
                                            If you have any concerns, you can ask us at our <a href="https://www.dreamyduodesigns.com/help">Help Desk</a>.
                                            <br>
                                            Or if you just wanted to give some feedBack, you can do that at our <a href="https://www.dreamyduodesigns.com/feedBack">Send FeedBack</a> page.
                                            <br>
                                            <br>
                                            This is an automated message.
                                            </p>
                                            `;
                                        }
                                        // DB.sendEmail_old(userEmail, subject, html);
                                    } catch (error2)
                                    {
                                        // Send Dev Error
                                        let subject = 'DB.SERVICE ERROR - /adminUpdateOrderStatus';
                                        console.log(subject);
                                        console.log(error2);
                                        console.log('Tried to search info from user_id:' + user_id);

                                        // Send Dev Error
                                        let html    = `<p>
                                        ${error2.message}
                                        <br><br>
                                        ${error2.stack}
                                        <b><b>
                                        Tried to search info from user_id: ${user_id}
                                        </p>`;
                                        const DB = DbService.getDbServiceInstance();
                                        // DB.sendEmail_old(process.env.AM_USER_DEV01, subject, html);
                                    }
                
                                    resolve(result.affectedRows);
                                }
                            });
                        }
                    });
                }
                else // status_id == 0 for delete
                {
                    const query = "DELETE FROM " + process.env.TABLE_ORDERS + " WHERE (order_id = ?);";
                    connection.query(query, [orderId], (err, result1) =>
                    {
                        if (err)
                        {
                            reject(err.message);
                        }
                        else
                        {
                            console.log('Order ' + orderId + ' has been deleted successfully');
                            resolve(result1.affectedRows);
                        }
                    });
                }
            } catch (error)
            {
                // Send Dev Error
                let subject = 'DB.SERVICE ERROR - /adminUpdateOrderStatus';
                console.log(subject);
                console.log(error);
                console.log(error.stack);

                let html    = `<p>
                ${error.message}
                <br><br>
                ${error.stack}
                </p>`;
                const DB = DbService.getDbServiceInstance();
                // DB.sendEmail_old(process.env.AM_USER_DEV01, subject, html);
            }
        });
        return response;
    }

    async admin_update_order_tracking(string_order_id, string_tracking_number, int_user_id)
    {
        const response = new Promise((resolve, reject) =>
        {
            // console.log(order_id);
            const DB = DbService.getDbServiceInstance();

            try
            {
                const query = "UPDATE " + process.env.TABLE_ORDERS + " SET tracking_number = ? WHERE order_id = ?;";
                connection.query(query, [string_tracking_number, string_order_id], (err, result) =>
                {
                    if (err)
                    {
                        reject(err.message);
                    }
                    else
                    {
                        // get user info ~ incomplete
                        const query2 = "SELECT * FROM " + process.env.TABLE_NAMES + " WHERE id = ?;";
                        connection.query(query2, [int_user_id], (err, result2) =>
                        {
                            if (err) 
                            {
                                console.log('Tried to search info from int_user_id:' + int_user_id);
                                reject(err);
                            }
                            else
                            {
                                result2 = result2[0];
                                console.table(result2);
    
                                console.log(`tracking_number ${string_order_id}: Updated to ${string_tracking_number}!`);
                                
                                // try
                                // {
                                //     let userEmail = result2.email;


                                //     // console.table(['userEmail', userEmail]);
        
                                //     let subject = `Order ${int_order_id}, Status: `;
                                //     let html = 
                                //     `
                                //     <h3>Your Order Status has been updated to </h3>
                                //     <p>
                                //     Visit <a href="https://www.dreamyduodesigns.com/orders">this link</a> to see your order status.
                                //     <br>
                                //     This is an automated message.
                                //     </p>
                                //     `;
                
                
                                //     if (status == 'Preparing Order')
                                //     {
                                //         // console.log('Status Case: 2');
                                //         html = 
                                //         `
                                //         <h3>Your Order Status has been updated to</h3>
                                //         <p>
                                //         This means that we have confirmed your payment, and your order is being prepared.
                                //         <br>
                                //         You will recieve a notification once your order status has been updated to 'Ready for Pickup'.
                                //         <br>
                                //         When your order is Ready for Pickup, you will be able to select your scheduled pick up day & time. Along with directions and an address.
                                //         <br>
                                //         <br>
                                //         Visit <a href="https://www.dreamyduodesigns.com/orders">www.dreamyduodesigns.com/orders</a> to see your order status.
                                //         <br>
                                //         <br>
                                //         This is an automated message.
                                //         </p>
                                //         `;
                                //     } 
                                //     // DB.sendEmail_old(userEmail, subject, html);
                                // } catch (error2)
                                // {
                                //     // Send Dev Error
                                //     let subject = 'DB.SERVICE ERROR - /adminUpdateOrderStatus';
                                //     console.log(subject);
                                //     console.log(error2);
                                //     console.log('Tried to search info from user_id:' + user_id);

                                //     // Send Dev Error
                                //     let html    = `<p>
                                //     ${error2.message}
                                //     <br><br>
                                //     ${error2.stack}
                                //     <b><b>
                                //     Tried to search info from user_id: ${user_id}
                                //     </p>`;
                                //     const DB = DbService.getDbServiceInstance();
                                //     // DB.sendEmail_old(process.env.AM_USER_DEV01, subject, html);
                                // }
            
                                resolve(result.affectedRows);
                            }
                        });
                    }
                });
            } catch (error)
            {
                // Send Dev Error
                let subject = 'DB.SERVICE ERROR - /adminUpdateOrderStatus';
                console.log(subject);
                console.log(error);
                console.log(error.stack);

                let html    = `<p>
                ${error.message}
                <br><br>
                ${error.stack}
                </p>`;
                const DB = DbService.getDbServiceInstance();
                // DB.sendEmail_old(process.env.AM_USER_DEV01, subject, html);
            }
        });
        return response;
    }

    async cancelOrder(order_id, email, password)
    {
        const DB = DbService.getDbServiceInstance();

        
        DB.getUserData(email, password).then((dataResult) =>
        {
            dataResult = dataResult;
            let userId    = dataResult.id
            let userEmail = dataResult.email;
            let userName = dataResult.name;

            const response = new Promise((resolve, reject) =>
            {
                let status = "Payment Required";
                // console.log(order_id);
                // console.log(email);
                // console.log(status);
        
                const query = "DELETE FROM " + process.env.TABLE_ORDERS + " WHERE (user_id = ?) AND (order_id = ?) AND (status = ?);";
                connection.query(query, [userId, order_id, status], (err, result1) =>
                {
                    if (err)
                    {
                        reject(err.message);
                    }
                    else
                    {

                        const query2 = "UPDATE " + process.env.TABLE_NAMES + " SET date_lastOrderPlaced = ? WHERE id = ?;";
                        const lastOrderPlaced = '';
                        connection.query(query2, [lastOrderPlaced, userId], (err, result2) =>
                        {
                            if (err) 
                            {
                                reject(err);
                            }
                            else
                            {
                                console.log(`Order ${order_id} for ${userName} has been canceled!`);
                                let subject = `(${order_id}) Order Canceled`;
                                let html = 
                                `
                                <h3>No need to worry. Order id: ${order_id} has been canceled. </h3>
                                <p>
                                Start your new order at <a href="${address}">dreamyduodesigns.com</a>.
                                <br>
                                This is an automated message.
                                </p>
                                `;
                                
                                // DB.sendEmail_old(userEmail, subject, html);

                                resolve([result1.affectedRows, result2.affectedRows]);
                            }
                        });
                    }
                })
            });
            return response;
        });
    }

    async admin_get_admin_config()
    {
        const response = await new Promise((resolve, reject) =>
        {
            const sql = "SELECT * FROM " + process.env.TABLE_ADMIN_CONFIG + ";";
            connection.query(sql, [], (error, results) =>
            {
                if (error)
                {
                    reject("/admin_get_admin_config ERROR:\n" + error);
                }
                else
                {
                    resolve(results[0]);
                }
            });
        });
        return response;
    }

    async admin_get_orders(search_limit)
    {
        const response = await new Promise((resolve, reject) => 
        {
            try
            {
                let sql_limit_string = '';
                if (search_limit != 'All')
                {
                    sql_limit_string = ` LIMIT ${search_limit}`;
                }


                const sql = `SELECT * FROM ${process.env.TABLE_ORDERS} ORDER BY status_id ASC, date_created DESC${sql_limit_string};`;
                
                // console.log(sql);
                connection.query(sql, [], (error, results) =>
                {
                    if (error)
                    {
                        reject(new Error("DBService.js ERROR\n" + error));
                    }
                    else if (typeof results == 'undefined') 
                    {
                        reject(".TABLE_ORDERS results are undefined.");
                    }
                    else
                    {
                        resolve(results);
                    }                   
                });
            }
            catch (error)
            {
                console.log(error);
                reject();
            }});
        return response;
    }

    async adminGetAccessCodes()
    {
        const response = await new Promise((resolve, reject) => 
        {
            try
            {
                const sql = "SELECT * FROM " + process.env.TABLE_CODES + ";";
                connection.query(sql, [], (error, results) =>
                {
                    if (error)
                    {
                        reject(new Error("DBService.js ERROR\n" + error));
                    }
                    if (typeof results == 'undefined') 
                    {
                        reject("Access Codes are undefined.");
                    }

                    let accessCodesText = "";
                    for (let i = 0; i < results.length; i++)
                    {
                        accessCodesText += results[i].code + "\n";
                        // console.log(results[i].code);
                    }
                    resolve(accessCodesText);
                })
            }
            catch (error)
            {
                console.log(error);
                reject();
            }});
        return response;
    }

    async admin_set_search_limit(search_limit)
    {
        const response = new Promise((resolve, reject) =>
        {
            try
            {
                const query  = `UPDATE ${process.env.TABLE_ADMIN_CONFIG}  SET \`search_limit\` = ? WHERE (\`id\` = \'1\');`;
                connection.query(query, [search_limit], (err, results) =>
                {
                    if (err)
                    {
                        reject(err.message);
                    }
                    else
                    {
                        resolve(results.affectedRows);
                    }
                });
            }
            catch (error)
            {
                reject('ERROR /admin_set_search_limit() \n' + error);
            }
        });
        return response;
    }

    async admin_set_menu(new_menu, date_menu_updated)
    {
        const response = new Promise((resolve, reject) =>
        {
            try
            {
                let sql_statement, values = new String();

                // delete previous menu
                sql_statement = 
                `
                SET SQL_SAFE_UPDATES = 0;
                DELETE FROM ${process.env.TABLE_ITEMS};
                SET SQL_SAFE_UPDATES = 1;
                ALTER TABLE ${process.env.TABLE_ITEMS} AUTO_INCREMENT = 1;
                INSERT INTO ${process.env.TABLE_ITEMS} (name, price, description) VALUES ?;
                `;
                // INSERT INTO ${process.env.TABLE_ITEMS} (name, price, description) VALUES ?;

                // insert new menu
                values = new_menu;
                console.log('New Menu:');
                console.log(values);

                connection.query(sql_statement, [values], (err, results) =>
                {
                    if (err)
                    {
                        reject(err.message);
                    }
                    else
                    {
                        sql_statement = "UPDATE " + process.env.TABLE_ADMIN_CONFIG  + " SET `menu_updated` = ? WHERE (`id` = '1');";
                        connection.query(sql_statement, [date_menu_updated], (err2, results2) =>
                        {
                            if (err2)
                            {
                                reject(err2.message);
                            }
                            else
                            {
                                resolve([results.affectedRows, results2.affectedRows]);
                            }
                        });
                    }
                });
            }
            catch (error)
            {
                reject('ERROR /admin_set_menu() \n' + error);
            }
        });
        return response;
    }

// CREATE TABLE `dreamyduodesigns`.`shirt_fire_dragon` (
//     `id` INT NOT NULL,
//     `style` LONGTEXT NULL,
//     `color` LONGTEXT NULL,
//     `size` LONGTEXT NULL,
//     `price` DOUBLE NULL,
//     `stock` INT NULL,
//     PRIMARY KEY (`id`));

// INSERT INTO `dreamyduodesigns`.`shirt_fire_dragon` (`id`, `style`, `color`, `size`, `price`, `stock`) VALUES ('1', 'Unisex', 'Black', 'XL', '25', '42');


    async admin_set_shirt_table(string_table_name, values)
    {
        const response = new Promise((resolve, reject) =>
        {
            try
            {
                // let sql_statement, values = new String();
                let sql_statement;

                // delete previous menu, then insert row
                sql_statement = 
                `
                SET SQL_SAFE_UPDATES = 0;
                DELETE FROM ${process.env.TABLE_SHIRTS};
                SET SQL_SAFE_UPDATES = 1;
                ALTER TABLE ${process.env.TABLE_SHIRTS} AUTO_INCREMENT = 1;
                INSERT INTO ${process.env.TABLE_SHIRTS} (name, style, color, size, discount, price, stock) VALUES ?;
                `;
                
                // create table, then insert row
                // sql_statement = 
                // `
                // CREATE TABLE ${string_table_name} (
                //     id INT NOT NULL AUTO_INCREMENT,
                //     style LONGTEXT NULL,
                //     color LONGTEXT NULL,
                //     size LONGTEXT NULL,
                //     price DOUBLE NULL,
                //     PRIMARY KEY (id));
                
                // INSERT INTO ${string_table_name} (style, color, size, price) VALUES (?);

                // `;


                // insert new menu
                // values = ['1', 'Unisex', 'Black', 'XL', '25', '42'];
                console.log('New Menu:');
                console.log(values);

                connection.query(sql_statement, [values], (err, results) =>
                {
                    if (err)
                    {
                        reject(err.message);
                    }
                    else
                    {
                        // sql_statement = "UPDATE " + process.env.TABLE_ADMIN_CONFIG  + " SET `menu_updated` = ? WHERE (`id` = '1');";
                        // connection.query(sql_statement, [date_menu_updated], (err2, results2) =>
                        // {
                        //     if (err2)
                        //     {
                        //         reject(err2.message);
                        //     }
                        //     else
                        //     {
                        //         resolve([results.affectedRows, results2.affectedRows]);
                        //     }
                        // });
                        resolve(results.affectedRows);
                    }
                });
            }
            catch (error)
            {
                reject('ERROR /admin_set_shirt_table() \n' + error);
            }
        });
        return response;
    }


    async forgotPasswordGenerateCode(email)
    {
        const response = new Promise((resolve, reject) =>
        {
            email = email.toLowerCase();
            const verificationCode  = Math.random().toString(36).substr(2,10); // Random 10 char alphanumeric
            const query = "UPDATE " + process.env.TABLE_NAMES + " SET verificationCode = ? WHERE email = ?;";
            connection.query(query, [verificationCode, email], (err, result) =>
            {
                if (err)
                {
                    reject(err.message);
                    return;
                }
                else
                {
                    if (result.affectedRows <= 0)
                    {
                        let error = 'forgotPasswordGenerateCode(email) ERROR: Email could not be found: ' + email;
                        reject(error);
                        return;
                    }

                    console.log(`ForgotPasswordGenerateCode Email: ${email} : ${verificationCode} verification code sent!`);
                    let subject = 'Forgot your Password';
                    let html = 
                    `
                    <h3>Verifcation Code: <b>${verificationCode}</b></h3>
                    <p>
                    Please enter this code to create your new password.
                    <br>
                    This is an automated message.
                    </p>
                    `;
                    
                    const DB = DbService.getDbServiceInstance();
                    // DB.sendEmail_old(email, subject, html);

                    resolve(result.affectedRows);
                }
            });
        });
        return response;
    }

    async send_verification_code(account_attributes)
    {
        const response = new Promise((resolve, reject) =>
        {
            let verification_code  = Math.random().toString(36).toUpperCase().substr(2,8); // Random 8 char alphanumeric - uppercase only

            const query = "UPDATE " + process.env.TABLE_NAMES + " SET verificationCode = ? WHERE id = ?";
            connection.query(query, [verification_code, account_attributes.id], (err, result) =>
            {
                if (err)
                {
                    reject(err.message);
                    return;
                }
                else
                {
                    if (result.affectedRows <= 0)
                    {
                        let error = 'send_verification_code(email) ERROR: Email could not be found.';
                        reject(error);
                        return;
                    }

                    console.log(`send_verification_code Email: ${account_attributes.email} : ${verification_code} verification code sent!`);
                    let subject = 'Verify Email';
                    let html = 
                    `
                    <h3>Verifcation Code: <b>${verification_code}</b></h3>
                    <p>
                    Please enter this code to verify your account.
                    <br>
                    This is an automated message.
                    </p>
                    `;
                    
                    // Send email
                    const DB = DbService.getDbServiceInstance();
                    let send_email_result = DB.sendEmail(account_attributes, subject, html);
                    send_email_result.then((result) => 
                    {
                        // console.table(result);
                        // console.table(result.response);
                        resolve(result.affectedRows);
                    })
                    .catch((result) =>
                    {
                        console.log('Email Delivery Failed');
                        console.log(result);
                        reject(result);
                    });
                }
            });
        });
        return response;
    }

    async updateAccountAttributes(account_attributes, new_email, new_name)
    {
        const response = await new Promise((resolve, reject) => 
        {
            new_email = new_email.toLowerCase();
            let sql_statement, values;

            // console.log('account_attributes.email: ' + account_attributes.email);
            // console.log('new_email: '                + new_email);
            // console.log('new_name: '                 + new_name);

            // if the user sent a new email
            if (account_attributes.email.toLowerCase() != new_email.toLowerCase())
            {
                // console.log('case 1');
                sql_statement = `UPDATE ${process.env.TABLE_NAMES} SET name = ?, email = ?, email_verified = ? WHERE id = ?;`;
                values = [new_name, new_email, 0, account_attributes.id];
            }
            else // if no new email, update only other attributes
            {
                // console.log('case 2');
                sql_statement = `UPDATE ${process.env.TABLE_NAMES} SET name = ? WHERE id = ?;`;
                //              UPDATE  `dreamyduodesigns`.`names` SET `name` = 'Marko' WHERE (`id` = '1');

                values = [new_name, account_attributes.id];
            }

            // console.log(sql_statement);
            // console.log(values);

            // SQL Update user info
            connection.query(sql_statement, values, (sql_error_1, sql_results_1) =>
            {
                if (sql_error_1)
                {
                    reject("DBService.js /updateAccountAttributes(email, password) sql_error_1\n" + sql_error_1.message);
                    console.trace(sql_error_1);
                }
                else
                {
                    console.log(`UpdateAccountAttributes Email: ${new_email} : Name: ${new_name} user account update sent!`);
                    let subject = 'Updated Account Information';
                    let html = 
                    `
                    <h3>Your account information has been updated at dreamyduodesigns.com</b></h3>
                    <p>
                    This is an automated message.
                    </p>
                    `;
                    
                    const DB = DbService.getDbServiceInstance();
                    // DB.sendEmail_old(new_email, subject, html);

                    // SQL Update user's latest order with new contact info
                    // sql_statement = `UPDATE ${process.env.TABLE_ORDERS} SET name = ?, email = ? WHERE user_id = ? ORDER BY date_created DESC LIMIT 1;`;
                    sql_statement = `UPDATE ${process.env.TABLE_ORDERS} SET name = ?, email = ? WHERE user_id = ?;`;
                    values = [new_name, new_email, account_attributes.id];
                    connection.query(sql_statement, values, (sql_error_2, sql_results_2) =>
                    {
                        if (sql_error_2)
                        {
                            reject("DBService.js /updateAccountAttributes(email, password) sql_error_2\n" + sql_error_2.message);
                            console.trace(sql_error_2);
                        }
                        else
                        {
                            // IF, RESULTS = TO OR GREATER THAN ONE RESULT THEN!!!

                            resolve([sql_results_1.affectedRows, sql_results_2.affectedRows]);
                        }
                    });
                }
            });
        });
        return response;
    }

    async updatePassword(email, password, verificationCode)
    {
        const response = new Promise((resolve, reject) =>
        {
            email = email.toLowerCase();
            let encryptedText = CryptoJS.AES.encrypt(password, process.env.KEY).toString();

            const query = "UPDATE " + process.env.TABLE_NAMES + " SET password = ? WHERE email = ? AND verificationCode = ?;";
            connection.query(query, [encryptedText, email, verificationCode], (err, result) =>
            {
                if (err)
                {
                    reject(err.message);
                }
                else if (result.affectedRows == 0)
                {
                    reject("Verification Code \nNo record match found");
                }
                else
                {
                    // console.log(result);
                    console.log(`updatePassword Email: ${email} : ${verificationCode} Password updated!`);
                    let subject = 'Your password has been updated';
                    let html = 
                    `
                    <p>
                        Your password has been updated at dreamyduodesigns.
                    <br>
                    This is an automated message.
                    </p>
                    `;
                    const DB = DbService.getDbServiceInstance();
                    // DB.sendEmail_old(email, subject, html);
                    resolve(result.affectedRows);
                }
            })
        });
        return response;
    }

    async submit_verification_code(account_attributes, verification_code)
    {
        const response = new Promise((resolve, reject) =>
        {

            const query = "UPDATE " + process.env.TABLE_NAMES + " SET email_verified = ? WHERE id = ? AND verificationCode = ?;";
            connection.query(query, [1, account_attributes.id, verification_code], (err, result) =>
            {
                if (err)
                {
                    console.log(err);
                    reject(err.message);
                }
                else if (result.affectedRows == 0)
                {
                    console.log("No record match found");
                    reject("No record match found");
                }
                else
                {
                    console.log('account verified!');
                    resolve(result.affectedRows);
                }
            })
        });
        return response;
    }

    async customerSupportEmailFeedBack(account_attributes, subject, description)
    {
        const response = new Promise((resolve, reject) =>
        {
            try
            {
                // send to both dreamyduodesigns@gmail, and user email
                let toEmail = `${process.env.AM_USER}, ${account_attributes.email}`;

                subject = `[FeedBack] ${subject}`;
                // <img style="width: 700px;" src="cid:header"/>
                let html = 
                `
                <h3>FeedBack Response:</h3>
                <p>
                    "${description}"
                <br>
                This is an automated message.
                </p>
                `;
                
                // Send email
                const DB = DbService.getDbServiceInstance();
                // // DB.sendEmail_old(toEmail, subject, html);
                resolve(true);

            } catch (error)
            {
                reject(error);
            }
        });
        return response;
    }

    async customerSupportEmailHelpDesk(account_attributes, order_id, description)
    {
        const response = new Promise((resolve, reject) =>
        {
            try
            {
                // send to both dreamyduodesigns@gmail, and user email
                let toEmail = `${process.env.AM_USER}, ${account_attributes.email}`;

                let subject = `[Help Desk] Order: ${order_id}`;
                let html = 
                `
                <h3>Help Desk Response:</h3>
                <p>
                    "${description}"
                <br>
                This is an automated message.
                </p>
                `;
                
                // Send email
                const DB = DbService.getDbServiceInstance();
                // DB.sendEmail_old(toEmail, subject, html);
                resolve(true);

            } catch (error)
            {
                reject(error);
            }
        });
        return response;
    }

    /*
    sendEmail(user_id, toEmail, subject, html, email_verified)
        try
            send email

            if email_verified != 0
                email_verified = 0
        
        catch(error)
            mysql_setEmailDeliveryFalse(user_id)
        
        woohoo
    */

    async email_error(account_attributes, error_name, error_message, error_lineNumber)
    {
        console.log('\n\n=============================================');

        console.log('DB/email_error');
        // send to both dreamyduodesigns@gmail, and user email
        let toEmail = `${process.env.AM_USER_DEV01}`;

        console.log('error_name:');
        console.log(error_name);

        console.log('error_message:');
        console.log(error_message);

        console.log('error_lineNumber:');
        console.log(error_lineNumber);

        console.table(account_attributes);

        let subject = `Error_Message`;
        let html = 
        `
        <h3>Error Details:</h3>
        <p>
            <b>error_name</b>:<br>
            ${error_name}<br>

            <b>error_message</b>:<br>
            ${error_message}<br>

            <b>error_lineNumber</b>:<br>
            ${error_lineNumber}<br>
        </p>
        `;
        
        // Send email
        const DB = DbService.getDbServiceInstance();
        // DB.sendEmail_old(toEmail, subject, html);
        console.log('\n\n=============================================');
    }


    async sendEmail(account_attributes, subject, html)
    {
        const response = new Promise((resolve, reject) =>
        {
            // console.table(account_attributes);

            let transporter = nodemailer.createTransport(
            {
                service: 'gmail',
                auth:
                {
                    user: process.env.AM_USER,
                    pass: process.env.AM_PASSWORD
                }
            });
            
            let mailOptions =
            {
                from:    process.env.AM_USER,
                to:      account_attributes.email,
                subject: subject,
                html:    html
                // attachments:
                // [{
                //     filename: 'header.png',
                //     path: __dirname +'/public/images/email/header.png', 
                //     cid: 'header' //same cid value as in the html img src
                // }]
            };
            
            transporter.sendMail(mailOptions, ( error, info ) =>
                {
                    // if error, set email delivery status on user account to failed (email_verified = 1)
                    if ( error )
                    {
                        console.log( 'Error: 111' );
                        reject( error );
                    }

                    else
                    {
                        resolve( info );
                    }
                });
        });
        return response;
    }

    async sendEmail_old(toEmail, subject, html)
    {
        let transporter = nodemailer.createTransport(
        {
            service: 'gmail',
            auth:
            {
                user: process.env.AM_USER,
                pass: process.env.AM_PASSWORD
            }
        });
        
        let mailOptions =
        {
            from:    process.env.AM_USER,
            to:      toEmail,
            subject: subject,
            html:    html
            // attachments:
            // [{
            //     filename: 'header.png',
            //     path: __dirname +'/public/images/email/header.png', 
            //     cid: 'header' //same cid value as in the html img src
            // }]
        };
        

        // console.table([
        //     ['AM_USER',     process.env.AM_USER],
        //     ['AM_PASSWORD', process.env.AM_PASSWORD]]);
        
        transporter.sendMail(mailOptions, ( error, info ) =>
        {
            if ( error )
            {
                console.log( error );

            }
            else
            {
                console.log( 'Email sent: ' + info.response );
            }
        });
    }
}

module.exports = DbService;