
var search_limit;
var dropdown_search_limit;
// var address = 'https://www.dreamyduodesigns.com';
var address = 'http://localhost:8040';
var orders                  = null; 
var new_order_notification  = false; 

document.addEventListener('DOMContentLoaded', () =>
{
    // console.log("DOMContentLoaded"); 
    // $('#inputSearchOrders').text('');
    dropdown_search_limit = $( '#dropdown_search_limit' );
    ready();
});
 
function display_account_attributes() 
{ 
    // try 
    // { 
    //     // get total of items  
    //     let cart        = data.cart[0][1]; 
    //     // let cart_points = data.cart_points.cart[0][1]; 
 
    //     let totalQty = cart; 
 
    //     if (data == null) 
    //     { 
    //         console.log('Error: No User Data'); 
    //         return; 
    //     } 
 
    //     // navbar 
    //     let cartQty = document.getElementById('cart-quantity'); 
    //     cartQty.dataset.quantity = totalQty; 
    //     $("#cart-quantity").text(totalQty); 
    // } 
    // catch (error) 
    // { 
    //     console.trace(error); 
    // } 
} 

function update_dropdown_search_limit(event)
{ 
    event                   = event.currentTarget;
    let new_selected_value  = $(event).attr("value"); 
    search_limit            = new_selected_value;

    console.log('attr value: ' + new_selected_value); 
    console.log('search_limit value: ' + search_limit);
 
    fetch(address + '/admin_set_search_limit', 
    { 
        credentials: "include", 
        method: 'PATCH', 
        headers: 
        { 
            'Content-type': 'application/json' 
        }, 
        body: JSON.stringify 
            ({ 
                search_limit: search_limit
            })
        }) 
        .then(response => response.json()) 
        .then((data) =>  
        {
            // Update Status to new option 
            $(dropdown_search_limit).text(new_selected_value);

            // Re-populate orders with new limit
            populateUserOrders(); 
        })
        .catch((error =>  
        { 
            console.log("update_dropdown_search_limit()  catch:" + error); 
    }));
}

// if you change code here, dont forget to update searchOrderClick(event) 
function populateUserOrders() 
{ 
    // reset notification flag 
    new_order_notification = false; 
 

    // populate page with new user orders 
    fetch(address + '/admin_get_orders', 
    { 
        credentials: "include", 
        method: 'PATCH', 
        headers: 
        { 
            'Content-type': 'application/json' 
        }, 
        body: JSON.stringify 
            ({ 
                search_limit: search_limit
            })
    })
    .then(response => response.json()) 
    .then(data =>   
    {
        orders = Array.from(data); 
        document.getElementById("orders-items").innerHTML = '';

        let searchText = $('#inputSearchOrders').val().trim().toLowerCase(); 
        let counter = 0; 

        try
        {
            for (let i = 0; i < orders.length; i++)
            { 
                console.log(orders[i]); 

                let userOrder = orders[i]; 

                let status_id              = userOrder.status_id;
                let int_user_id            = userOrder.user_id;
                let status                 = userOrder.status;
                let string_order_id        = userOrder.order_id;
                let name                   = userOrder.name;
                let email                  = userOrder.email;
                let cart                   = JSON.parse(userOrder.cart);
                let total                  = userOrder.total;
                let date_created           = new Date(userOrder.date_created);
                let payment_method         = userOrder.payment_method;
                let string_tracking_number = userOrder.tracking_number;
                let description            = userOrder.description;

                // description replace '+' with line breaks
                description = description.replace(/\+/g, '<br>' );

                console.log('string_tracking_number ' + string_tracking_number);

                let options = 
                { 
                    hour: '2-digit', 
                    minute: '2-digit', 
                    year: "numeric", 
                    month: "2-digit", 
                    day: "2-digit" 

                }; 
                let string_data_attributes = `data-string_order_id="${string_order_id}" data-status_id="${status_id}"  data-status="${status}" data-name="${name}" data-int_user_id="${int_user_id}" data-total="${total}" data-date_created="${new Date(date_created).toISOString()}" data-string_tracking_number="${string_tracking_number}"`;  

                date_created = date_created.toLocaleString('en-us', options); 

                // if order does not contain status, string_order_id, name, or email,
                // or if cart does not contain search text
                // ,skip

                if (   !status.toLowerCase().includes(searchText)         && !string_order_id.toLowerCase().includes(searchText)  
                    && !name.toLowerCase().includes(searchText)           && !email.toLowerCase().includes(searchText)
                    && !payment_method.toLowerCase().includes(searchText) && !description.toLowerCase().includes(searchText))
                { 
                    // console.log('TRUE THO'); 
                    continue;
                }
                else 
                { 
                    counter += 1; 
                } 

                // Payment Required - Order will not be made until this is paid. 
                // Preparing Order - Order is in queue to be baked. 
                // Ready for Pickup - Order has been made, and waiting for pickup. 
                // Complete - Order has been delivered. 


                let statusText = status; 
                let pickup_text = ''; 
                let dropDownButton = ''; 

                if (status_id == 1) // == 'Payment Required' 
                { 
                    dropDownButton =  
                    ` 
                    <button  id="selected-${string_order_id}" class="btn btn-danger btn-sm dropdown-toggle rounded-pill w-100" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="max-width: 225px;" >${statusText}</button> 
                    `; 

                    status =  
                    ` 
                    <button name="Cancel Order" class="dropdown-item" type="button" onClick="dropDownUpdateOrderStatus(event);">Cancel Order</button> 
                    <button class="dropdown-item disabled">Payment Required</button> 
                    <button name="Preparing Order"  class="dropdown-item" type="button" onClick="dropDownUpdateOrderStatus(event);">Preparing Order</button> 
                    <button name="Ready for Pickup" class="dropdown-item" type="button" onClick="dropDownUpdateOrderStatus(event);">Ready for Pickup</button> 
                    <button name="Complete"         class="dropdown-item" type="button" onClick="dropDownUpdateOrderStatus(event);">Complete</button> 
                    `; 
                } else if (status_id == 2) // == 'Payment Required' 
                { 
                    dropDownButton =  
                    ` 
                    <button  id="selected-${string_order_id}" class="btn btn-primary btn-sm dropdown-toggle rounded-pill w-100" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="max-width: 225px;" >${statusText}</button> 
                    `; 

                    status =  
                    ` 
                    <button name="Cancel Order" class="dropdown-item" type="button" onClick="dropDownUpdateOrderStatus(event);">Cancel Order</button> 
                    <button class="dropdown-item disabled">Payment Required</button> 
                    <button class="dropdown-item disabled">Preparing Order</button> 
                    <button name="Ready for Pickup" class="dropdown-item" type="button" onClick="dropDownUpdateOrderStatus(event);">Ready for Pickup</button> 
                    <button name="Complete"         class="dropdown-item" type="button" onClick="dropDownUpdateOrderStatus(event);">Complete</button> 
                    `; 
                } else if (status_id == 3) // == 'Ready for Pickup' 
                { 
                    dropDownButton =  
                    ` 
                    <button  id="selected-${string_order_id}" class="btn btn-warning btn-sm dropdown-toggle rounded-pill w-100" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="max-width: 225px;" >${statusText}</button> 
                    `; 

                    try  
                    {
                        let pickup_scheduled = new Date(userOrder.pickup_scheduled);

                        if (userOrder.pickup_scheduled != null) 
                        { 
                            console.log(pickup_scheduled.toISOString()); 
                            pickup_text = `<p style="font-weight: normal;">*Pickup Selected*</p>`; 
                        } 
                    }  
                    catch (error) 
                    { 
                        console.trace(error);     
                    } 
                    // console.log(pickup_text); 

                    status =  
                    ` 
                    <button name="Cancel Order" class="dropdown-item" type="button" onClick="dropDownUpdateOrderStatus(event);">Cancel Order</button> 
                    <button class="dropdown-item disabled">Payment Required</button> 
                    <button class="dropdown-item disabled">Preparing Order</button> 
                    <button class="dropdown-item disabled">Ready for Pickup</button> 
                    <button name="Complete"         class="dropdown-item" type="button" onClick="dropDownUpdateOrderStatus(event);">Complete</button> 
                    `; 
                } else if (status_id == 4) //  == 'Complete') 
                { 
                    dropDownButton =  
                    ` 
                    <button  id="selected-${string_order_id}" class="btn btn-success btn-sm dropdown-toggle rounded-pill w-100" aria-expanded="false" data-bs-toggle="dropdown" type="button" style="max-width: 225px;" >${statusText}</button> 
                    `; 
                    status =  
                    ` 
                    <button class="dropdown-item disabled">Cancel Order</button> 
                    <button class="dropdown-item disabled">Payment Required</button> 
                    <button class="dropdown-item disabled">Preparing Order</button> 
                    <button class="dropdown-item disabled">Ready for Pickup</button> 
                    <button class="dropdown-item disabled">Complete</button> 
                    `; 
                } 

                let card = '';
                card += 
                ` 
                <div class="product"> 
                    <div id='row_order_${string_order_id}' class="row product-image d-flex justify-content-between align-items-start" ${string_data_attributes} > 

                        <div class="col-md-4 product-info"> 
                            <div class="product-specs d-flex flex-column  align-items-center"> 
                                <div class="w-100" style="padding: 0px 0px 15px 0px; text-align: center;"> 
                                    <span>Status</span> 
                                    <br> 
                                    <div class="dropdown w-100" >
                                        ${dropDownButton}
                                        <div class="dropdown-menu w-100" name="${string_order_id}">
                                        ${status} 
                                        </div>
                                    </div>
                                    ${pickup_text} 
                                </div> 

                                <div style="padding: 0px 0px 15px 0px; text-align: center;"> 
                                    <span>Order ID</span> 
                                    <br> 
                                    <span class="value">${string_order_id}</span> 
                                </div> 

                                <div style="padding: 0px 0px 15px 0px; text-align: center;"> 
                                    <span>Payment Method</span>
                                    <br>
                                    <span class="value">${payment_method}</span> 
                                </div> 

                                <div style="padding: 0px 0px 15px 0px; text-align: center;"> 
                                    <span>Total</span> 
                                    <br> 
                                    <span class="value">$${total}</span> 
                                </div> 
                                
                            </div> 
                        </div> 

                        <div class="col-md-4 product-info"> 
                            <div class="product-specs d-flex flex-column  align-items-center"> 
                                <div style="padding: 0px 0px 15px 0px; text-align: center;"> 
                                    <span>Items</span> 
                                    <br> 
                                    <span class="value">${description}</span> 
                                </div> 
                            </div> 
                        </div> 


                        <div class="col-md-4 product-info "> 
                            <div class="product-specs d-flex flex-column  align-items-center"> 

                                <div style="padding: 0px 0px 15px 0px; text-align: center;"> 
                                    <span>Tracking Number</span>
                                    <br>
                                    <span class="value">
                                        <button  id="buttonUpdate" type="button" class="btn btn-sm btn-primary rounded-pill w-100" onclick="modal_click(event)">Update Tracking #</button>
                                    </span>
                                    <br>
                                    <span><a id='tracking_number_${string_order_id}' href="${string_tracking_number}" style="text-decoration: none;">${string_tracking_number}</a></span> 
                                </div>

                                <div style="padding: 0px 0px 15px 0px; text-align: center;"> 
                                    <span>Date Created</span> 
                                    <br> 
                                    <span class="value">${date_created}</span> 
                                </div> 


                                <div style="padding: 0px 0px 15px 0px; text-align: center;"> 
                                    <span>Name</span> 
                                    <br> 
                                    <span class="value">${name}</span> 
                                </div> 

                                <div style="padding: 0px 0px 15px 0px; text-align: center;"> 
                                    <span>Email</span> 
                                    <br> 
                                    <span class="value">${email}</span> 
                                </div> 

                            </div>
                        </div>

                    </div> 
                </div> 
                `; 
                // create card 
                let HTML_orders = $('#orders-items');
                HTML_orders.append(card);
            }
        }
        catch (error)
        {
            // console.trace(error);
            // console.log(error.message);

            let error_message = error.name + '\n' + error.message + '\n' +  error.lineNumber;
            console.log(error_message);

            // Notification
            const message    = "Error: Order could not be loaded";
            const alertType  = 'danger';
            const iconChoice = 3;
            let   duration   = 3
            alertNotify(message, alertType, iconChoice, duration);
    
            // reload page
            duration *= 1000;
            setTimeout(() =>
            {
                fetch( address + '/email_error',
                    {
                        credentials: "include",
                        method: 'POST',
                        headers: {
                            'Content-type': 'application/json'
                        },
                        body: JSON.stringify( {
                            error_name: error.name,
                            error_message: error.message,
                            error_lineNumber: error.lineNumber
                        } )
                    } )
                    .then( response => response.json() )
                    .then( data =>
                    {
                        if ( data == true )
                        {
                            // Notification
                            const message = "Error Message Sent Success";
                            const alertType = 'primary';
                            const iconChoice = 3;
                            alertNotify( message, alertType, iconChoice, 3 );
                        }

                        else
                        {
                            // Notification
                            const message = "Error Message Sent Failed";
                            const alertType = 'info';
                            const iconChoice = 3;
                            alertNotify( message, alertType, iconChoice, 3 );
                        }
                    } );
            }, duration);
        }
        $('#inputSearchLabel').text(`(${counter})`); 

        console.log('orders.length'); 
        console.log(orders.length.toString()); 
    }); 
} 

function dropDownUpdateOrderStatus(event) 
{ 
    // console.log('start dropDownUpdateOrderStatus(event)'); 
 
    event                 = event.currentTarget; 
    let parentDiv         = event.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode; 
    let orderId           = $(parentDiv).attr("data-string_order_id"); 
    let int_user_id       = $(parentDiv).attr("data-int_user_id"); 
    let status_id         = null; 
    let newSelectedStatus = $(event).attr("name"); 
 
    console.log('attr name: ' + newSelectedStatus); 
    // console.log('attr data-status_id: ' + status_id); 
 
    let dropDownSubElementID = $(`#selected-${orderId}`);  
    // console.log(dropDownSubElementID); 
 
 
    if (confirm('Are you sure you want to update this order status?\nThis user will be notified.\nThis action cannot be undone')) 
    { 
        // console.log('Update order pressed.'); 
    }  
    else 
    { 
        // console.log('Cancel action pressed.'); 
        return; 
    } 
 
    // $(dropDownSubElementID).text(status); 
     
    if (newSelectedStatus == 'Cancel Order') 
    { 
        status_id = 0; 
    } 
    else if (newSelectedStatus == 'Preparing Order') 
    { 
        status_id = 2; 
    }  
    else if (newSelectedStatus == 'Ready for Pickup') 
    { 
        status_id = 3; 
    }  
    else if (newSelectedStatus == 'Complete') 
    { 
        status_id = 4; 
    } 
 
    fetch(address + '/adminUpdateOrderStatus', 
    { 
        credentials: "include", 
        method: 'PATCH', 
        headers: 
        { 
            'Content-type': 'application/json' 
        }, 
        body: JSON.stringify( 
            { 
                orderId:    orderId, 
                status_id:  status_id, 
                status:     newSelectedStatus, 
                int_user_id:    int_user_id 
            }) 
    }) 
    .then(response => response.json()) 
    .then((data) =>  
    { 
        // Update Status to new option 
        $(dropDownSubElementID).text(newSelectedStatus); 
 
        // disable used option 
        $(event).addClass("disabled"); 
        $(event).on(('onClick'), null); 
 
        // console.log('Status check: ' + status); 
 
        // console.log("status.includes('complete')"); 
        // console.log(status.includes('complete')); 
 
        // console.log('New Status ID: ' + status_id); 
 
 
        // if 'Cancel Order' 
        if (status_id == 0) 
        { 
            dropDownSubElementID.removeClass("btn-danger"); 
            dropDownSubElementID.removeClass("btn-primary"); 
            dropDownSubElementID.removeClass("btn-warning"); 
            dropDownSubElementID.addClass("btn-dark"); 
            dropDownSubElementID.addClass("disabled"); 
            $(dropDownSubElementID).empty(); 
            $(dropDownSubElementID).text('Order Canceled'); 
        } 
        // if 'Preparing Order' 
        else if (status_id == 2) 
        { 
            // console.log(1); 
            dropDownSubElementID.removeClass("btn-danger"); 
            dropDownSubElementID.addClass("btn-primary"); 
        } 
        // if 'Ready for Pickup' 
        else if (status_id == 3) 
        { 
            // console.log(2); 
            dropDownSubElementID.removeClass("btn-danger"); 
            dropDownSubElementID.removeClass("btn-primary"); 
            dropDownSubElementID.addClass("btn-warning"); 
        } 
        // if 'Complete' 
        else if (status_id == 4) 
        { 
            // console.log(3); 
            dropDownSubElementID.removeClass("btn-danger"); 
            dropDownSubElementID.removeClass("btn-primary"); 
            dropDownSubElementID.removeClass("btn-warning"); 
            dropDownSubElementID.addClass("btn-success"); 
        } 
 
        // console.log("dropDownUpdateOrderStatus(event) complete"); 
    }).catch((error) =>  
    { 
        console.log("dropDownUpdateOrderStatus(event) ERROR catch:" + error); 
    }); 
}

function radioOrdersClick() 
{ 
    // console.log('hello');
    $('#codes-items').attr('hidden', ''); 
    $('#chart-items').attr('hidden', ''); 
 
    $('#orders-items-outer').removeAttr('hidden'); 
 
 
    // set radioOrders active 
    document.getElementById("radioOrders").classList.add("active"); 
 
    document.getElementById("radioCodes").classList.remove("active"); 
    document.getElementById("radioChart").classList.remove("active"); 
    document.getElementById("radio_sale").classList.remove("active");

    // Remove search text value 
    $('#inputSearchOrders').val(''); 
    populateUserOrders(); 
} 
 
function radioCodesClick() 
{ 
    $('#orders-items-outer').attr('hidden', ''); 
    $('#codes-items').removeAttr('hidden'); 
    $('#chart-items').attr('hidden', ''); 
    $('#sale-items').attr('hidden', ''); 
 
    // set radioCodes active 
    document.getElementById("radioCodes").classList.add("active"); 
 
    document.getElementById("radioOrders").classList.remove("active"); 
    document.getElementById("radioChart").classList.remove("active"); 
    document.getElementById("radio_sale").classList.remove("active");

 
    fetch(address + '/adminGetAccessCodes') 
    .then(response => response.json()) 
    .then((response) => 
    { 
        $('#activeAccessCodes').val(response['data']); 
    }) 
    .catch(() => 
    { 
        $('#activeAccessCodes').val(); 
    }); 
} 
 
function radioChartClick() 
{ 
    $('#codes-items').attr('hidden', ''); 
    $('#orders-items-outer').attr('hidden', ''); 
    $('#chart-items').removeAttr('hidden'); 
    $('#sale-items').attr('hidden', ''); 
 
 
 
    // set radioChart active 
    document.getElementById("radioChart").classList.add("active"); 
 
    document.getElementById("radioOrders").classList.remove("active"); 
    document.getElementById("radioCodes").classList.remove("active"); 
    document.getElementById("radio_sale").classList.remove("active");



    // update chart 
    fetch(address + '/admin_get_orders', 
    { 
        credentials: "include", 
        method: 'PATCH', 
        headers: 
        { 
            'Content-type': 'application/json' 
        }, 
        body: JSON.stringify 
            ({ 
                search_limit: "All"
            })
    })
    .then(response => response.json()) 
    .then(data =>   
    { 
        orders = Array.from(data['data']); 
 
 
        let chartMapPaymentRequired = new Map(); // (id, {quantity: 5, name: 'Krispy Treats'}) 
        let chartMapPaymentRecieved = new Map(); // (id, {quantity: 5, name: 'Krispy Treats'}) 
        let chartMapReadyForPickup = new Map(); // (id, {quantity: 5, name: 'Krispy Treats'}) 

 
        let chartMapTotal = new Map(); // will be used to combine the two above Maps 
 
 
        for (let i = 0; i < orders.length; i++) 
        { 
            console.log(orders[i]); 
 
            let userOrder = orders[i]; 
            let status_id = userOrder.status_id; 
            let cart      = JSON.parse(userOrder.cart); 
 
            // create chartMap 
            // loop the data, and build the chart
            if (status_id == 1) // == 'Payment Required')
            { 
                for (let j = 1; j < cart.length; j++) 
                { 
                    let cartElement = cart[j];
                    console.log('cartElement: ');
                    console.log(cartElement);
                    let id = cartElement[0]; 
                    let name = cartElement[2]; 
                    console.log('cartElement name: ');
                    console.log(name);
                    // if item exists in chartMapPaymentRequired, add to its quantity 
                    if (chartMapPaymentRequired.has(id)) 
                    { 
                        console.log(`chartMapPaymentRequired.has(${id}) TRUE`); 
                        console.log(chartMapPaymentRequired.get(id));
                        let quantity = chartMapPaymentRequired.get(id).quantity; 
                        let name     = chartMapPaymentRequired.get(id).name; 
     
                        let newQuantity = quantity + cartElement[1]; 
     
                        chartMapPaymentRequired.set(id, {quantity: newQuantity, name: name}); 
                    } 
                    // if item does not exist in chartMapPaymentRequired, add its entry into the chartMapPaymentRequired 
                    else 
                    { 
                        // console.log(`chartMapPaymentRequired.has(${id}) FALSE`); 
                        chartMapPaymentRequired.set(id, {quantity: cartElement[1], name: cartElement[2]}); 
                    } 
                } 
            } 
            else if (status_id == 2) // == 'Preparing Order') 
            { 
                for (let j = 1; j < cart.length; j++) 
                { 
                    let cartElement = cart[j];
                    console.log('cartElement: ');
                    console.log(cartElement);
                    let id = cartElement[0]; 
                    let name = cartElement[2]; 
                    console.log('cartElement name: ');
                    console.log(name);
                    // if item exists in chartMapPaymentRecieved, add to its quantity 
                    if (chartMapPaymentRecieved.has(id)) 
                    { 
                        // console.log(`chartMapPaymentRecieved.has(${id}) TRUE`); 
                        let quantity = chartMapPaymentRecieved.get(id).quantity; 
                        let name     = chartMapPaymentRecieved.get(id).name; 
     
                        let newQuantity = quantity + cartElement[1]; 
     
                        chartMapPaymentRecieved.set(id, {quantity: newQuantity, name: name}); 
                    } 
                    // if item does not exist in chartMapPaymentRecieved, add its entry into the chartMapPaymentRecieved 
                    else 
                    { 
                        // console.log(`chartMapPaymentRecieved.has(${id}) FALSE`); 
                        chartMapPaymentRecieved.set(id, {quantity: cartElement[1], name: cartElement[2]}); 
                    }
                }
            }
            else if (status_id == 3) // == 'Ready for Pickup') 
            { 
                for (let j = 1; j < cart.length; j++) 
                { 
                    let cartElement = cart[j];
                    console.log('cartElement: ');
                    console.log(cartElement);
                    let id = cartElement[0]; 
                    let name = cartElement[2]; 
                    console.log('cartElement name: ');
                    console.log(name);
                    // if item exists in chartMapReadyForPickup, add to its quantity 
                    if (chartMapReadyForPickup.has(id)) 
                    { 
                        // console.log(`chartMapReadyForPickup.has(${id}) TRUE`); 
                        let quantity = chartMapReadyForPickup.get(id).quantity; 
                        let name     = chartMapReadyForPickup.get(id).name; 
     
                        let newQuantity = quantity + cartElement[1]; 
     
                        chartMapReadyForPickup.set(id, {quantity: newQuantity, name: name}); 
                    } 
                    // if item does not exist in chartMapReadyForPickup, add its entry into the chartMapReadyForPickup 
                    else 
                    { 
                        // console.log(`chartMapReadyForPickup.has(${id}) FALSE`); 
                        chartMapReadyForPickup.set(id, {quantity: cartElement[1], name: cartElement[2]}); 
                    }
                }
            }
        } 

        // sort the map chartMapPaymentRequired, for the total
        chartMapPaymentRequired = new Map([...chartMapPaymentRequired].sort()); 
        for (let [key, value] of chartMapPaymentRequired.entries()) 
        { 
            // if item exists in chartMapTotal, add to its quantity 
            if (chartMapTotal.has(key)) 
            { 
                // console.log(`chartMapTotal.has(${key}) TRUE`); 
                let quantity = chartMapTotal.get(key).quantity; 
                let name     = chartMapTotal.get(key).name; 
 
                let newQuantity = quantity + value.quantity; 
 
                chartMapTotal.set(key, {quantity: newQuantity, name: name}); 
            } 
            // if item does not exist in chartMapTotal, add its entry into the chartMapTotal 
            else 
            { 
                chartMapTotal.set(key, {quantity: value.quantity, name: value.name}); 
            } 
        } 

         
        // sort the map chartMapPaymentRecieved, for the total
        chartMapPaymentRecieved = new Map([...chartMapPaymentRecieved].sort()); 
        for (let [key, value] of chartMapPaymentRecieved.entries()) 
        {              
            // if item exists in chartMapTotal, add to its quantity 
            if (chartMapTotal.has(key)) 
            { 
                let quantity = chartMapTotal.get(key).quantity; 
                let name     = chartMapTotal.get(key).name; 
 
                let newQuantity = quantity + value.quantity; 
 
                chartMapTotal.set(key, {quantity: newQuantity, name: name}); 
            } 
            // if item does not exist in chartMapTotal, add its entry into the chartMapTotal 
            else 
            { 
                chartMapTotal.set(key, {quantity: value.quantity, name: value.name}); 
            } 
        } 

        
        // sort the map chartMapReadyForPickup, for the total
        chartMapReadyForPickup = new Map([...chartMapReadyForPickup].sort());
        for (let [key, value] of chartMapReadyForPickup.entries())
        {
            // if item exists in chartMapTotal, add to its quantity
            if (chartMapTotal.has(key))
            { 
                let quantity = chartMapTotal.get(key).quantity;
                let name     = chartMapTotal.get(key).name;
 
                let newQuantity = quantity + value.quantity;
 
                chartMapTotal.set(key, {quantity: newQuantity, name: name});
            }
            // if item does not exist in chartMapTotal, add its entry into the chartMapTotal 
            else
            {
                chartMapTotal.set(key, {quantity: value.quantity, name: value.name}); 
            }
        }

        let tableHTML = '';
        // console.log('chartMapTotal BEFORE sort:');
        // console.table(chartMapTotal);


        chartMapTotal = new Map([...chartMapTotal].sort()); 
        // console.log('chartMapTotal AFTER sort:');
        // console.table(chartMapTotal);

        for (let [key, value] of chartMapTotal.entries())
        {
            let quantityRequired       = 0;
            let quantityRecieved       = 0;
            let quantityReadyForPickup = 0;
 
            if (chartMapPaymentRequired.has(key))
            { 
                quantityRequired = chartMapPaymentRequired.get(key).quantity; 
            }
            if (chartMapPaymentRecieved.has(key))
            { 
                quantityRecieved = chartMapPaymentRecieved.get(key).quantity; 
            }
            if (chartMapReadyForPickup.has(key))
            { 
                quantityReadyForPickup = chartMapReadyForPickup.get(key).quantity; 
            }
 
            tableHTML += 
                    ` 
                    <tr style="font-size: smaller;"> 
                        <th style="font-weight: normal; font-size: smaller;">${value.name}</th> 
                        <td class="table-danger"  >${quantityRequired}</td> 
                        <td class="table-primary" >${quantityRecieved}</td>
                        <td class="table-warning" >${quantityReadyForPickup}</td> 
                        <td style="font-weight: bold;">${value.quantity}</td> 
                    </tr> 
                    `;
        } 

        document.getElementById("chartTable").innerHTML = tableHTML;
    }); 
} 

function radio_sale_click() 
{
    // hide other sections
    $('#orders-items-outer').attr('hidden', '');
    $('#codes-items').attr('hidden', '');
    $('#chart-items').attr('hidden', '');

    // show selected section
    $('#sale-items').removeAttr('hidden');
 
 
    // set radioOrders active 
    document.getElementById("radio_sale").classList.add("active");

    document.getElementById("radioOrders").classList.remove("active");
    document.getElementById("radioCodes").classList.remove("active");
    document.getElementById("radioChart").classList.remove("active");

    // get, and display config settings
    fetch(address + '/admin_get_admin_config') 
    .then(response => response.json()) 
    .then(data =>   
    {
        // console.table(data);
        
        // grab inputs
        let start_date  = new Date(data.sale_start);
        let end_date    = new Date(data.sale_end);

        // display locale date_time result on Label
        let locale_options = { year: 'numeric', month: 'numeric', day: 'numeric', hour: '2-digit', minute: '2-digit' };
        let string_result1 = start_date.toLocaleTimeString([], locale_options);
        let string_result2 = end_date.toLocaleTimeString([], locale_options);

        document.getElementById('label_start_result').innerText = string_result1;
        document.getElementById('label_end_result').innerText   = string_result2;

    });

    // Remove all rows from table_shirts
    $("#table_shirts_body tr").remove();

    // HTML paired with table_shirts function add_row
    fetch(address + '/get_table_shirts') 
    .then(response => response.json()) 
    .then(data =>   
    {
        data = data['data'];
        // console.table(data);
        let html_row;
        for (let i = 0; i < data.length; i++)
        {
            html_row +=
            `
            <tr> <!-- Row -->
            <td > <!-- Cell 'X' -->
                <div class="input-group">
                    <button type="button" class="btn btn-secondary btn-sm" style="margin: auto;" onclick="button_table_shirts_remove_row(this)">X</button>
                </div>
            </td>
    
            <td > <!-- product_name -->
                <div class="input-group">
                    <input name='cell_shirts_name' type="text" class="form-control" style="font-size: smaller;" placeholder="product_name" onchange="validate_table_shirts()" autocomplete="off" maxlength="60" value='${data[i].name}'>
                </div>
            </td>
    
            <td > <!-- style -->
                <div class="input-group">
                    <input name='cell_shirts_style' type="text" class="form-control" style="font-size: smaller;" placeholder="style" onchange="validate_table_shirts()" autocomplete="off" maxlength="60" value='${data[i].style}'>
                </div>
            </td>
    
            <td > <!-- color -->
                <div class="input-group">
                    <input name='cell_shirts_color' type="text" class="form-control" style="font-size: smaller;" placeholder="color" onchange="validate_table_shirts()" autocomplete="off" maxlength="60" value='${data[i].color}'>
                </div>
            </td>
    
            <td > <!-- size -->
                <div class="input-group">
                    <input name='cell_shirts_size' type="text" class="form-control" style="font-size: smaller;" placeholder="size" onchange="validate_table_shirts()" autocomplete="off" maxlength="60" value='${data[i].size}'>
                </div>
            </td>
    
            <td style="max-width: 10rem;"> <!-- discount -->
                <div class="input-group">
                <span class="input-group-text" style="font-size: smaller;">%</span>
                    <input name='cell_shirts_discount' type="number" class="form-control" style="font-size: smaller;" placeholder="0" step="5.00" min="0" max="100" maxlength="3" onchange="validate_table_shirts()" autocomplete="off"value='${data[i].discount}'>
                </div>
            </td>

            <td style="max-width: 10rem;"> <!-- price -->
                <div class="input-group">
                    <span class="input-group-text" style="font-size: smaller;">$</span>
                    <input name='cell_shirts_price' type="number" class="form-control" style="font-size: smaller;" placeholder="0.00" step="1.00" min="0" max="9999" onchange="validate_table_shirts()" autocomplete="off" value='${data[i].price}'>
                </div>
            </td>

            <td style="max-width: 10rem;"> <!-- stock -->
                <div class="input-group">
                    <input name='cell_shirts_stock' type="number" class="form-control" style="font-size: smaller;" placeholder="0" step="1.00" min="0" max="9999" onchange="validate_table_shirts()" autocomplete="off" value='${data[i].stock}'>
                </div>
            </td>

        </tr>

        `;
        }
    
        //     <tr> <!-- Row -->
        //     <td > <!-- Cell 'X' -->
        //         <div class="input-group">
        //             <button type="button" class="btn btn-secondary btn-sm" style="margin: auto;" onclick="button_table_menu_remove_row(this)">X</button>
        //         </div>
        //     </td>
        //     <td > <!-- Cell Name -->
        //         <div class="input-group">
        //             <input name='cell-name' type="text" class="form-control" style="font-size: smaller;" placeholder="Treat" onchange="validate_table_menu()" autocomplete="off" maxlength="60" value='${data[i].name}'>
        //         </div>
        //     </td>
        //     <td style="max-width: 10rem;"> <!-- Cell Price -->
        //         <div class="input-group">
        //             <span class="input-group-text" style="font-size: smaller;">$</span>
        //             <input name='cell-price' type="number" class="form-control" style="font-size: smaller;" placeholder="0.00" step="0.01" min="0" max="9999" onchange="validate_table_menu()" autocomplete="off" value='${data[i].price}'>
        //         </div>
        //     </td>
        // </tr>

        // add the new row to the table
        $('#table_shirts_body').append(html_row);
    
        // select newly added row
        let rows_cell_shirts_discount = document.querySelectorAll('input[name="cell_shirts_discount"]');
        let rows_cell_shirts_price    = document.querySelectorAll('input[name="cell_shirts_price"]');
        let rows_cell_shirts_stock    = document.querySelectorAll('input[name="cell_shirts_stock"]');

        for(let i = 0; i < rows_cell_shirts_price.length; i++)
        {
            // Apply input restriction to currency input cell
            setInputFilter(rows_cell_shirts_discount[i], ( value ) => /^((\d+(\.\d*)?)|(\.\d+))$/.test( value ), "Must be a number value");
            setInputFilter(rows_cell_shirts_price[i],    ( value ) => /^((\d+(\.\d*)?)|(\.\d+))$/.test( value ), "Must be a number value");
            setInputFilter(rows_cell_shirts_stock[i],    ( value ) => /^((\d+(\.\d*)?)|(\.\d+))$/.test( value ), "Must be a number value");
        }
    });
}

function generateAccessCodes() 
{ 
    if (confirm('This action will create (10) more access codes. Are you sure?')) 
    { 
        // continue 
        // console.log('Confirm pressed.'); 
    } else 
    { 
        // stop 
        // console.log('Cancel action pressed.'); 
        return; 
    } 
 
    $('#activeAccessCodes').val(); 
 
    fetch(address + '/generateAccessCodes', 
    { 
        credentials: "include", 
        method: 'POST' 
    }) 
    .then(response => response.json()) 
    .then((response) => 
    { 
        radioCodesClick(); 
    }); 
} 
 
function getOrdinalSuffix(dt) 
{ 
    return (dt.getDate() % 10 == 1 && dt.getDate() != 11 ? 'st' : (dt.getDate() % 10 == 2 && dt.getDate() != 12 ? 'nd' : (dt.getDate() % 10 == 3 && dt.getDate() != 13 ? 'rd' : 'th')));  
} 
 
function check_new_orders() 
{ 
    // if no notification is sent yet, keep checking for new orders 
    if (new_order_notification == false) 
    { 
        fetch(address + '/admin_get_orders', 
        { 
            credentials: "include", 
            method: 'PATCH', 
            headers: 
            { 
                'Content-type': 'application/json' 
            }, 
            body: JSON.stringify 
                ({ 
                    search_limit: search_limit
                })
        })
        .then(response => response.json()) 
        .then(data =>   
        { 
            let new_order_count = Array.from(data['data']).length; 
     
            // if the new orders count is higher than the current, and the notification has not been sent already 
            // play notification sound 
            if (new_order_count > orders.length) 
            { 
                let audio = new Audio('/audio/money.mp3'); 
                audio.play(); 
     
                // Notification 
                const message       = `New Customer Order!`; 
                const alertType     = 'primary'; 
                const iconChoice    = 2; 
                const duration      = 5; 
                alertNotify(message, alertType, iconChoice, duration); 
     
                // update notification sent flag to true 
                new_order_notification = true; 
            } 
            // console.table(['new_order_notification', new_order_notification]); 
        }); 
    } 
    // check for new orders every 10 seconds 
    setTimeout(check_new_orders, 10000); 
} 
 
function alertNotify(message, alertType, iconChoice, duration) 
{ 
    if (iconChoice == 1)      // ✔ 
        iconChoice = 'check-circle-fill'; 
    else if (iconChoice == 2) // i 
        iconChoice = 'info-fill'; 
    else if (iconChoice == 3) // ! 
        iconChoice = 'exclamation-triangle-fill'; 
 
    let iconHTML = `<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="${alertType}}:"><use xlink:href="#${iconChoice}"/></svg>`; 
    alertType = `alert-${alertType}`; 
 
    let html =  
    ` 
    <div id="alertNotification" class="alert ${alertType}  text-center  col-auto" style="margin: 0 auto; align-text: center;" role="alert"> 
        <span> 
            ${iconHTML} 
            ${message} 
        </span> 
    </div> 
    `; 
 
    // show pop up 
    $('#notification').append(html); 
     
    duration *= 1000; 
    setTimeout(() =>
    {
        $( "#alertNotification" ).remove();
    }, duration); 
} 
 
function ready() 
{ 
    // get search limit from admin config
    fetch(address + '/admin_get_admin_config') 
    .then(response => response.json()) 
    .then(data =>   
    {
        search_limit = new String(data.search_limit);
        dropdown_search_limit.text(search_limit);

        // load orders
        try
        {
            // get account attributes
            // let div_account_attributes  = document.getElementById('div_account_attributes');
            // let account_attributes      = JSON.parse(div_account_attributes.getAttribute('data-account_attributes'));
            // let cart_total    = account_attributes.cart[0][1]; 

            radioOrdersClick();
            check_new_orders();
        }
        catch (error) 
        {
            console.trace(error); 
        }
    })
    .catch((error) =>
    {
        console.trace(error); 
    });
} 

// SALE FUNCTIONS //
function validate_table_menu()
{
    let table_data_valid = false;

    // get table
    let table_menu = document.getElementById('table_menu');

    // get table data
    let table_menu_length = table_menu.rows.length - 1;
    let list_names        = document.getElementsByName('cell-name');
    let list_price        = document.getElementsByName('cell-price');
    // let list_description  = document.getElementsByName('cell-description');

    // loop through data 
    try
    {
        for (let i = 0; i < table_menu_length; i++)
        {
            let cell_name        = list_names[i].value;
            let cell_price       = list_price[i].value;
            // let cell_description = list_description[i].value;
    
            // if all cells on row are not empty
            if (cell_name.length > 0 && cell_price.length > 0)
            {
                // table data valid
                table_data_valid = true;
            }
            else
            {
                // table data not valid
                table_data_valid = false;
                break;
            }
            console.table([cell_name, cell_price]);
        }
    }
    catch (error)
    {
        table_data_valid = false;
    }

    // console.log('table_data_valid: ' + table_data_valid);
    if (table_data_valid == true)
    {
        // if table data is valid, enable button
        document.getElementById('button_update_menu').disabled = false;
    }
    else
    {
        // if table data is not valid, disable button
        document.getElementById('button_update_menu').disabled = true;
    }
}

// HTML paired with panel button button_sale on load
function button_menu_add_row()
{
    let html_row =
    `
    <tr> <!-- Row -->
        <td > <!-- Cell 'X' -->
            <div class="input-group">
                <button type="button" class="btn btn-secondary btn-sm" style="margin: auto;" onclick="button_table_menu_remove_row(this)">X</button>
            </div>
        </td>
        <td > <!-- Cell Name -->
            <div class="input-group">
                <input name='cell-name' type="text" class="form-control" style="font-size: smaller;" placeholder="Treat" onchange="validate_table_menu()" autocomplete="off" maxlength="60">
            </div>
        </td>
        <td style="max-width: 10rem;"> <!-- Cell Price -->
            <div class="input-group">
                <span class="input-group-text" style="font-size: smaller;">$</span>
                <input name='cell-price' type="number" class="form-control" style="font-size: smaller;" placeholder="0.00" step="0.01" min="0" max="9999" onchange="validate_table_menu()" autocomplete="off">
            </div>
        </td>
    </tr>
    `;
/*
<td > <!-- Cell Description -->
    <div class="input-group">
        <input name='cell-description' type="text" class="form-control" placeholder="tasty" onchange="validate_table_menu()" autocomplete="off" maxlength="200">
    </div>
</td>
*/


    // Disable Save Menu Button, since blank row is added
    document.getElementById('button_update_menu').disabled = true;

    // add the new row to the table
    $('#table_body').append(html_row);

    // select newly added row
    let last_row = document.querySelectorAll('input[name="cell-price"]');

    // select currency input from new row
    let input_currency = last_row[last_row.length-1];

    // Apply input restriction to currency input cell
    setInputFilter(input_currency, ( value ) => /^((\d+(\.\d*)?)|(\.\d+))$/.test( value ), "Must be a currency value");
}

function button_table_menu_remove_row(button)
{
    button.parentNode.parentNode.parentNode.remove();
    validate_table_menu();
}

// SALE FUNCTIONS //
function validate_table_shirts()
{
    let table_data_valid = false;

    // get table
    let table_shirts = document.getElementById('table_shirts');

    console.log(`table_shirts.rows.length[${table_shirts.rows.length - 1}]`);


    // get table data
    let table_shirts_length = table_shirts.rows.length - 1;
    let list_name         = document.getElementsByName('cell_shirts_name');
    let list_style        = document.getElementsByName('cell_shirts_style');
    let list_color        = document.getElementsByName('cell_shirts_color');
    let list_size         = document.getElementsByName('cell_shirts_size');
    let list_discount     = document.getElementsByName('cell_shirts_discount');
    let list_price        = document.getElementsByName('cell_shirts_price');
    let list_stock        = document.getElementsByName('cell_shirts_stock');


    // loop through data 
    try
    {
        for (let i = 0; i < table_shirts_length; i++)
        {
            let cell_name        = list_name[i].value;
            let cell_style       = list_style[i].value;
            let cell_color       = list_color[i].value;
            let cell_size        = list_size[i].value;

            // discount
            // Remove trailing zeros ex: "0000 => 0"
            list_discount[i].value = parseInt(list_discount[i].value);
            if (list_discount[i].value > 100)  { list_discount[i].value = 100; }
            else if (list_discount[i].value < 0) { list_discount[i].value = 0; }
            let cell_discount    = list_discount[i].value;

            // price
            // make price trailing two decimals ex: '0 => 0.00'
            if (list_price[i].value > 1000)  { list_price[i].value = 1000.00; }
            else if (list_price[i].value < 0) { list_price[i].value = 0.00; }
            list_price[i].value  = parseFloat(list_price[i].value).toFixed(2);
            let cell_price       = list_price[i].value;

            // stock
            // Remove trailing zeros ex: "0000 => 0"
            list_stock[i].value = parseInt(list_stock[i].value);
            if (list_stock[i].value > 1000)  { list_stock[i].value = 1000; }
            else if (list_stock[i].value < 0) { list_stock[i].value = 0; }
            let cell_stock       = list_stock[i].value;



    
            // if all cells on row are not empty
            if (cell_name.length > 0 && cell_style.length > 0 && cell_color.length > 0 && cell_size.length > 0 && cell_discount.length > 0 && cell_price.length > 0 && cell_stock.length > 0)
            {
                // table data valid
                table_data_valid = true;
            }
            else
            {
                // table data not valid
                table_data_valid = false;
                // break;
            }
            console.table([cell_name, cell_style, cell_color, cell_size, cell_discount, cell_price, cell_stock]);
        }
    }
    catch (error)
    {
        console.trace(error);
        table_data_valid = false;
    }

    console.log('table_data_valid: ' + table_data_valid);
    if (table_data_valid == true)
    {
        // if table data is valid, enable button
        document.getElementById('button_table_shirts_apply_changes').disabled = false;
    }
    else
    {
        // if table data is not valid, disable button
        document.getElementById('button_table_shirts_apply_changes').disabled = true;
    }
}

// HTML paired with panel button button_sale on load
function button_table_shirts_add_row()
{
    let html_row =
    `
    <tr> <!-- Row -->
        <td > <!-- Cell 'X' -->
            <div class="input-group">
                <button type="button" class="btn btn-secondary btn-sm" style="margin: auto;" onclick="button_table_shirts_remove_row(this)">X</button>
            </div>
        </td>

        <td > <!-- product_name -->
            <div class="input-group">
                <input name='cell_shirts_name' type="text" class="form-control" style="font-size: smaller;" placeholder="product_name" onchange="validate_table_shirts()" autocomplete="off" maxlength="60">
            </div>
        </td>

        <td > <!-- style -->
            <div class="input-group">
                <input name='cell_shirts_style' type="text" class="form-control" style="font-size: smaller;" placeholder="style" onchange="validate_table_shirts()" autocomplete="off" maxlength="60">
            </div>
        </td>

        <td > <!-- color -->
            <div class="input-group">
                <input name='cell_shirts_color' type="text" class="form-control" style="font-size: smaller;" placeholder="color" onchange="validate_table_shirts()" autocomplete="off" maxlength="60">
            </div>
        </td>

        <td > <!-- size -->
            <div class="input-group">
                <input name='cell_shirts_size' type="text" class="form-control" style="font-size: smaller;" placeholder="size" onchange="validate_table_shirts()" autocomplete="off" maxlength="60">
            </div>
        </td>

        <td style="max-width: 10rem;"> <!-- discount -->
            <div class="input-group">
            <span class="input-group-text" style="font-size: smaller;">%</span>
                <input name='cell_shirts_discount' type="number" class="form-control" style="font-size: smaller;" placeholder="0" step="5.00" min="0" max="100" onchange="validate_table_shirts()" autocomplete="off" value="0">
            </div>
        </td>

        <td style="max-width: 10rem;"> <!-- price -->
            <div class="input-group">
                <span class="input-group-text" style="font-size: smaller;">$</span>
                <input name='cell_shirts_price' type="number" class="form-control" style="font-size: smaller;" placeholder="0.00" step="1.00" min="0" max="9999" onchange="validate_table_shirts()" autocomplete="off" value="0.00">
            </div>
        </td>

        <td style="max-width: 10rem;"> <!-- stock -->
            <div class="input-group">
                <input name='cell_shirts_stock' type="number" class="form-control" style="font-size: smaller;" placeholder="0" step="1.00" min="0" max="9999" onchange="validate_table_shirts()" autocomplete="off" value="0">
            </div>
        </td>

    </tr>
    `;

    // Disable Save Menu Button, since blank row is added
    document.getElementById('button_table_shirts_apply_changes').disabled = true;

    // add the new row to the table
    $('#table_shirts_body').append(html_row);

    // select newly added row
    // let last_row = document.querySelectorAll('input[name="cell_shirts_price"]');

    // // select currency input from new row
    // let input_currency = last_row[last_row.length-1];

    // // Apply input restriction to currency input cell
    // setInputFilter(input_currency, function(value) { return /^((\d+(\.\d*)?)|(\.\d+))$/.test(value); }, "Must be a currency value");


    // // select newly added row
    let rows_cell_shirts_discount = document.querySelectorAll('input[name="cell_shirts_discount"]');
    let rows_cell_shirts_price    = document.querySelectorAll('input[name="cell_shirts_price"]');
    let rows_cell_shirts_stock    = document.querySelectorAll('input[name="cell_shirts_stock"]');
    // select currency input from new row
    let input_discount = rows_cell_shirts_discount[rows_cell_shirts_discount.length - 1];
    let input_price    = rows_cell_shirts_price[rows_cell_shirts_price.length - 1];
    let input_stock    = rows_cell_shirts_stock[rows_cell_shirts_stock.length - 1];

    // Apply input restriction to currency input cell
    setInputFilter(input_discount, ( value ) => /^((\d+(\.\d*)?)|(\.\d+))$/.test( value ), "Must be a number value");
    setInputFilter(input_price,    ( value ) => /^((\d+(\.\d*)?)|(\.\d+))$/.test( value ), "Must be a number value");
    setInputFilter(input_stock,    ( value ) => /^((\d+(\.\d*)?)|(\.\d+))$/.test( value ), "Must be a number value");

    // select newly added row
    // let rows_cell_shirts_discount = document.querySelectorAll('input[name="cell_shirts_discount"]');
    // let rows_cell_shirts_price    = document.querySelectorAll('input[name="cell_shirts_price"]');
    // let rows_cell_shirts_stock    = document.querySelectorAll('input[name="cell_shirts_stock"]');

    // for(let i = 0; i < rows_cell_shirts_price.length; i++)
    // {
    //     // Apply input restriction to currency input cell
    //     setInputFilter(rows_cell_shirts_discount[i], function(value) { return /^((\d+(\.\d*)?)|(\.\d+))$/.test(value); }, "Must be a number value");
    //     setInputFilter(rows_cell_shirts_price[i],    function(value) { return /^((\d+(\.\d*)?)|(\.\d+))$/.test(value); }, "Must be a number value");
    //     setInputFilter(rows_cell_shirts_stock[i],    function(value) { return /^((\d+(\.\d*)?)|(\.\d+))$/.test(value); }, "Must be a number value");
    // }

}

function admin_set_menu()
{
    if (!confirm('Confirm: Press OK to update menu')) return;

    // get table
    let table_shirts = document.getElementById('table_shirts');

    // get table data
    let table_shirts_length = table_shirts.rows.length - 1;
    let list_names        = document.getElementsByName('cell-name');
    let list_price        = document.getElementsByName('cell-price');
    // let list_description  = document.getElementsByName('cell-description');

    let new_menu = [];

    // loop through data 
    for (let i = 0; i < table_shirts_length; i++)
    {
        let cell_name        = list_names[i].value;
        let cell_price       = list_price[i].value;
        // let cell_description = list_description[i].value;

        // new_menu.push([cell_name, cell_price, cell_description]);
        // empty description to prevent 'null' text on menu
        new_menu.push([cell_name, cell_price, ' ']);
        // console.table([cell_name, cell_price, cell_description]);
    }
    console.table(new_menu);


    fetch(address + '/admin_set_menu', 
    { 
        credentials: "include", 
        method: 'PATCH', 
        headers: 
        { 
            'Content-type': 'application/json' 
        }, 
        body: JSON.stringify 
        ({ 
            new_menu: new_menu,
            date_menu_updated: new Date()
        }) 
    })
    .then(response => response.json()) 
    .then((result) =>  
    {
        if (result == true)
        {
            // Notification 
            const message       = `Menu Updated!`; 
            const alertType     = 'success'; 
            const iconChoice    = 2; 
            const duration      = 3; 
            alertNotify(message, alertType, iconChoice, duration);
        }
        else
        {
            // Notification 
            const message       = `Something went wrong!`; 
            const alertType     = 'danger'; 
            const iconChoice    = 2; 
            const duration      = 4;
            alertNotify(message, alertType, iconChoice, duration);
        }

    }).catch((error =>  
    { 
        console.log("admin_set_menu()  catch:" + error);
        // Notification 
        const message       = `Something went wrong!`; 
        const alertType     = 'danger'; 
        const iconChoice    = 2; 
        const duration      = 4;
        alertNotify(message, alertType, iconChoice, duration); 
    }));

    // disable button 
    document.getElementById('button_update_menu').disabled = true;
}

function button_table_shirts_remove_row(button)
{
    button.parentNode.parentNode.parentNode.remove();
    validate_table_shirts();
}

function admin_set_table_shirts()
{
    if (!confirm('Confirm: Press OK to update table_shirts')) return;

    // get table
    let table_shirts = document.getElementById('table_shirts');

    // get table data
    let table_shirts_length = table_shirts.rows.length - 1;
    let list_name         = document.getElementsByName('cell_shirts_name');
    let list_style        = document.getElementsByName('cell_shirts_style');
    let list_color        = document.getElementsByName('cell_shirts_color');
    let list_size         = document.getElementsByName('cell_shirts_size');
    let list_discount     = document.getElementsByName('cell_shirts_discount');
    let list_price        = document.getElementsByName('cell_shirts_price');
    let list_stock        = document.getElementsByName('cell_shirts_stock');

    // let list_description  = document.getElementsByName('cell_shirts_description');

    let new_table_shirts = [];
    let array_names = [];

    // loop through data 
    for (let i = 0; i < table_shirts_length; i++)
    {
        let cell_name        = list_name[i].value;
        let cell_style       = list_style[i].value;
        let cell_color       = list_color[i].value;
        let cell_size        = list_size[i].value;
        let cell_discount    = list_discount[i].value;
        let cell_price       = list_price[i].value;
        let cell_stock       = list_stock[i].value;

        // empty description to prevent 'null' text on table_shirts
        // array_names.push('shirt_' + cell_name);
        // new_table_shirts.push([cell_style, cell_color, cell_size, cell_price]);

        new_table_shirts.push([cell_name, cell_style, cell_color, cell_size, cell_discount, cell_price, cell_stock]);

        // console.table([cell_name, cell_price, cell_description]);
    }
    console.table(new_table_shirts);
    // console.log(array_names[0]);
    // console.log(new_table_shirts[0]);

    fetch(address + '/admin_set_shirt_table', 
    { 
        credentials: "include", 
        method: 'PATCH', 
        headers: 
        { 
            'Content-type': 'application/json' 
        }, 
        body: JSON.stringify 
        ({ 
            string_table_name: array_names[0],
            values: new_table_shirts
        }) 
    })
    // date_table_shirts_updated: new Date()
    .then(response => response.json()) 
    .then((result) =>  
    {
        if (result == true)
        {
            // Notification 
            const message       = `table_shirts Updated!`; 
            const alertType     = 'success'; 
            const iconChoice    = 2; 
            const duration      = 3; 
            alertNotify(message, alertType, iconChoice, duration);
        }
        else
        {
            // Notification 
            const message       = `Something went wrong!`; 
            const alertType     = 'danger'; 
            const iconChoice    = 2; 
            const duration      = 4;
            alertNotify(message, alertType, iconChoice, duration);
        }

    }).catch((error =>  
    { 
        console.log("admin_set_table_shirts()  catch:" + error);
        // Notification 
        const message       = `Something went wrong!`; 
        const alertType     = 'danger'; 
        const iconChoice    = 2; 
        const duration      = 4;
        alertNotify(message, alertType, iconChoice, duration); 
    }));

    // disable button 
    // document.getElementById('button_update_table_shirts').disabled = true;
}

function modal_apply()
{
    // console.log($("#modal_input").val());
    let string_order_id            = $('#modal_body_2').attr('data-string_order_id');
    let int_user_id                = $("#modal_body_2").attr('data-int_user_id');
    let string_new_tracking_number = $('#modal_input').val();


    fetch(address + '/admin_update_order_tracking',
    { 
        credentials: "include", 
        method: 'PATCH' ,
        headers: { "Content-Type": "application/json", },
        body: JSON.stringify(
        {
            string_order_id:        string_order_id,
            string_tracking_number: string_new_tracking_number,
            int_user_id:            int_user_id
        })
    })
    .then(response => response.json()) 
    .then(data =>   
    {
        console.log('response: ' + data);
        if (data == true)
        {
            let modal_body_text = 
            `
            <div id='modal_body_2' class='modal-content' data-string_order_id="${string_order_id}" data-int_user_id="${int_user_id}" data-string_tracking_number="${string_new_tracking_number}">
                <div class='modal-body'>
                    <p>Current Tracking Number: <a href='https://www.ups.com/r/${string_new_tracking_number}' target='_blank'>${string_new_tracking_number}</a></p>
                    <input type='text' class='form-control' id='modal_input' placeholder='new tracking number'>
                </div>
            </div>
            `;

            // update parent html with new tracking number
            $(`#row_order_${string_order_id}`).attr('data-string_tracking_number', string_new_tracking_number);

            
            $(`#tracking_number_${string_order_id}`).text(string_new_tracking_number);


            

            $("#modal_input").val('');
            let modal_body   = document.getElementById( 'modal_body' );
            modal_body.innerHTML   = modal_body_text;
        }
        else
        {

        }
    });
}

function modal_close()
{
    $('#modal1').modal('hide');
}

function modal_click(event)
{
    event               = event.currentTarget;
    let html_parent_div = event.parentNode.parentNode.parentNode.parentNode.parentNode; 


    let string_order_id        = $(html_parent_div).attr("data-string_order_id");
    let int_user_id            = $(html_parent_div).attr("data-int_user_id");
    let string_tracking_number = $(html_parent_div).attr("data-string_tracking_number"); 

    let modal_title_text = string_order_id;

    let modal_body_text = 
    `
    <div id='modal_body_2' class='modal-content' data-string_order_id="${string_order_id}" data-int_user_id="${int_user_id}" data-string_tracking_number="${string_tracking_number}">
        <div class='modal-body'>
            <p>Current Tracking Number: <a href='https://www.ups.com/r/${string_tracking_number}' target='_blank'>${string_tracking_number}</a></p>
            <input type='text' class='form-control' id='modal_input' placeholder='new tracking number'>
        </div>
    </div>
    `;

    let modal_footer_text = 
    `
        <button type='button' class='btn btn-primary rounded-pill' onclick='modal_apply()' >Apply</button>
        <button type='button' class='btn btn-primary rounded-pill' onclick='modal_close()' >Close</button>
    `;


    let modal_title  = document.getElementById( 'modal_title' );
    let modal_body   = document.getElementById( 'modal_body' );
    let modal_footer = document.getElementById( 'modal_footer' );

    
    modal_title.innerHTML  = modal_title_text;
    modal_body.innerHTML   = modal_body_text;
    modal_footer.innerHTML = modal_footer_text;


    $('#modal1').modal('show');
}



// Restricts user input for the given textbox to the given inputFilter.
function setInputFilter(textbox, inputFilter, errMsg)
{
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop", "focusout"].forEach(( event ) =>
    {
        textbox.addEventListener( event, function ( e )
        {
            try
            {
                if ( inputFilter( this.value ) )
                {
                    // Accepted value
                    if ( [ "keydown", "mousedown", "focusout" ].indexOf( e.type ) >= 0 )
                    {
                        this.classList.remove( "input-error" );
                        this.setCustomValidity( "" );
                    }
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                }
                else if ( this.hasOwnProperty( "oldValue" ) )
                {
                    // Rejected value - restore the previous one
                    this.classList.add( "input-error" );
                    this.setCustomValidity( errMsg );
                    this.reportValidity();
                    this.value = this.oldValue;
                    this.setSelectionRange( this.oldSelectionStart, this.oldSelectionEnd );
                }

                else
                {
                    // Rejected value - nothing to restore
                    this.value = "";
                }
            }
            catch ( error )
            {
            }
        } );
    });
}