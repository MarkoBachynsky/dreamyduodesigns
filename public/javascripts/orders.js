document.addEventListener('DOMContentLoaded', () =>
{
    // console.log("DOMContentLoaded");
    ready();
});

// var address = 'https://www.dreamyduodesigns.com';
var address = 'http://localhost:8040';

// Update Cart Quantity
function loadCartTotal(data)
{
    try
    {
        // get total of items 
        let cart        = data.cart[0][1];
        // let cart_points = data.cart_points.cart[0][1];

        let totalQty = cart;

        if (data == null)
        {
            console.log('Error: No User Data');
            return;
        }

        // navbar
        let cartQty = document.getElementById('cart-quantity');
        cartQty.dataset.quantity = totalQty;
        $("#cart-quantity").text(totalQty);
    }
    catch (error)
    {
        console.log(error);
    }
}

function populateUserOrders()
{
    // get user order info
    fetch(address + '/get_user_orders')
    .then(response => response.json())
    .then(data =>  
    {
        // console.log(data);
        let orders = Array.from(data);
        // console.log(22222);

        for (let index = 0; index < orders.length; index++)
        {
            console.log(orders[index]);

            let userOrder = orders[index];

            let status_id              = userOrder.status_id;
            let string_order_id        = userOrder.order_id;
            let name                   = userOrder.name;
            let status                 = userOrder.status;
            let cart                   = JSON.parse(userOrder.cart);
            let total                  = userOrder.total;
            let payment_method         = userOrder.payment_method;
            let description            = userOrder.description;
            let string_tracking_number = userOrder.tracking_number;

            // description replace '+' with line breaks
            description = description.replace(/\+/g, '<br>' );

            // if null, make it empty string
            try { pickup_scheduled.length; } catch (error) { pickup_scheduled = ''; }

            let date_created = new Date(userOrder.date_created);
            let options =
            {
                hour: '2-digit',
                minute: '2-digit',
                year: "numeric",
                month: "2-digit",
                day: "2-digit"

            };
            // console.log("date_created");
            // console.log(date_created);
            date_created = date_created.toLocaleString('en-us', options);
            // console.log(date_created);

            let interactDiv = '', locationDiv = '';
            let dataAttributes = `data-string_order_id="${string_order_id}" data-status-id="${status}"`;
            let icon_HTML = 
            `
            <button type="button" class="btn btn-outline-primary btn-sm" data-status-id="${status_id}" onclick="popup_modal_status_info(event);" style="width: 1.5rem; height: 1.5rem; border-radius: 50rem; border-width: 1.25px; font-size: 0; padding: 0;" >
                <svg width="14" height="14" role="img" aria-label="info-fill}:">
                    <use xlink:href="#info-fill"/>
                </svg>
            </button>`;

            if (status == "Payment Required")
            {
                // status += ' ' + icon_HTML
                + '<br>' + '<p>(Include your <b>Order ID</b>)</p>';
                interactDiv =
                `
                <br>
                <span class="value">
                    <button name="cancel-order-button" ${dataAttributes} class="btn btn-warning btn-sm rounded-pill w-100" type="button" style="max-width: 225px;" >Cancel Order</button>
                </span>`;
            }

            else if (status == "Preparing Order")
            {
                // status += ' ' + icon_HTML;
            }

            else if (status == "Ready for Pickup")
            {
                
            } // end of 'Ready for Pickup'

            let cartText = "";
            for (let j = 1; j < cart.length; j++)
            {
                let cartElement = cart[j];
                cartText += "<b>[" + cartElement[1] + "]</b> " + cartElement[2] + "<br>";
            }

            let card = "";

            // if status is not complete, add info button
            if (status_id != 4)
            {
                status += ' ' + icon_HTML;
            }

            // create element in shopping cart
            card +=
            `
            <div class="product">
                <div class="row product-image justify-content-center align-items-start" style="padding: 0px;">

                    <div class="col-md-4 product-info">
                        <div class="product-specs d-flex flex-column  align-items-center">
                                
                            <div style="padding: 0px 0px 15px 0px; text-align: center;">
                                <span>Status</span>
                                <br>
                                <span class="value">${status}</span>
                            </div>

                            <div style="padding: 0px 0px 15px 0px; text-align: center;">
                                <span>Order ID</span>
                                <br>
                                <span class="value">${string_order_id}</span>
                            </div>

                            <div style="padding: 0px 0px 15px 0px; text-align: center;">
                                <span>Payment Method</span>
                                <br>
                                <span class="value">${payment_method}</span>
                            </div>

                            <div style="padding: 0px 0px 15px 0px; text-align: center;">
                                <span>Total</span>
                                <br>
                                <span class="value">$${total}</span>
                            </div>
                            
                        </div>
                    </div>

                    <div class="col-md-4 product-info">
                        <div class="product-specs d-flex flex-column  align-items-center">
                            
                            <div style="padding: 0px 0px 15px 0px; text-align: center;">
                                <span>Items</span>
                                <br>
                                <span class="value">${description}</span>
                            </div>

                        </div>
                    </div>


                    <div class="col-md-4 product-info ">
                        <div class="product-specs d-flex flex-column  align-items-center">

                            <div style="padding: 0px 0px 15px 0px; text-align: center;"> 
                                <span>Tracking Number</span>
                                <br>
                                <span><a id='tracking_number_${string_order_id}' href="${string_tracking_number}" style="text-decoration: none;">${string_tracking_number}</a></span> 
                            </div>


                            <div style="padding: 0px 0px 15px 0px; text-align: center;">
                                <span>Date Created</span>
                                <br>
                                <span class="value">${date_created}</span>
                            </div>

                        <div class="w-100" style="padding: 0px 0px 15px 0px; text-align: center;">
                            <br>
                            ${interactDiv}
                        </div>

                        </div>
                    </div>

                </div>
            </div>
            `;


            // create card
            let myform = $('#cart-items');
            myform.append(card);
        }

        let cancelOrderButtons = document.getElementsByName('cancel-order-button');
        for (let i = 0; i < cancelOrderButtons.length; i++)
        {
            // console.log("button found!");
            let button = cancelOrderButtons[i];
            button.addEventListener('click', buttonCancelOrder);
            // console.log("button " + i + " online!");
        }
        });
}

function buttonCancelOrder(event)
{
    if (confirm('Are you sure you want to cancel this order? \nThis action cannot be undone.')) {
        // 
        console.trace('Cancel order pressed.');
      } else {
        // 
        console.trace('Do not cancel order pressed.');
        return;
      }


    // console.log("\n" + "buttonCancelOrder(event)");
    let button = event.target;
    let string_order_id = button.dataset.string_order_id;

    console.log("string_order_id:\t" + string_order_id);

    fetch(address + '/cancelOrder',
        {
            credentials: "include",
            method: 'DELETE',
            headers:
            {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(
                {
                    string_order_id: string_order_id
                })
        })
        .then(response => response.json())
        .then(() => 
        {
            let parentDiv = button.parentNode.parentNode.parentNode.parentNode.parentNode;
            parentDiv.remove();
            // console.log("cancelOrder(event) complete");
        }).catch((error => 
        {
            console.log("cancelOrder(event)  catch:" + error);
        }));

}

function ready()
{
    // get cart total
    fetch(address + '/getUserData')
    .then(response => response.json())
    .then(data => 
    {
        loadCartTotal(data['data']);
        populateUserOrders();
    })
    .catch((error) =>
    {
        console.log(error)
    });
}

function getOrdinalSuffix(dt)
{
    return (dt.getDate() % 10 == 1 && dt.getDate() != 11 ? 'st' : (dt.getDate() % 10 == 2 && dt.getDate() != 12 ? 'nd' : (dt.getDate() % 10 == 3 && dt.getDate() != 13 ? 'rd' : 'th'))); 
}

function popup_modal_status_info(event)
{
    event          = event.currentTarget;
    let status_id  = $(event).attr("data-status-id");
    console.log('data-status-id value: ' + status_id);
 
    let modal_title = document.getElementById('modal_title');
    let modal_body = document.getElementById('modal_body');

    let modal_title_text, modal_body_text;

    if (status_id == '1')       // Payment Required
    {
        modal_title_text = `🚀 Payment Required 🚀`;
        modal_body_text = 
        `
        <h4>What's next?</h4>
        <br>
        <p style="font-size: smaller;">
            Your order has been <b>submitted</b> and is <b>awaiting payment</b><br>
            <br>
            Once confirmed your order status will be updated to '<b>Preparing Order</b>'<br>
            <br>
            - Payment can be made via <b>Venmo</b>, <b>Apple Pay</b>, or <b>Cash at Pickup</b><br>
            <br>
            - Payment is <b>due by end of sale</b>, or your order may be canceled<br>
            <br>
            - Cash at Pickup is <b>due at pickup</b><br>
            <br>
            <b>Include your Order ID</b> when paying via <a href="https://account.venmo.com/pay?txn=pay&recipients=Astro-Medibles">Venmo</a>, or Apple Pay<br>
            <br>
            Only one order can be placed per sale, but you can cancel and place a new order as needed
        </p>
        `;
    }
    else if (status_id == '2')  // Preparing Order
    {
        modal_title_text = `🚀 Preparing Order 🚀`;
        modal_body_text = 
        `
        <h4>What's next?</h4>
        <br>
        <p style="font-size: smaller;">
            We have confirmed your payment, and <b>your order is being prepared</b><br>
            <br>
            You will recieve a <b>notification via email</b> once it is ready<br>
            <br>
            When your order is '<b>Ready for Pickup</b>' you can:<br>
            - Schedule your <b>pick up day & time</b><br>
            - Select <b>the location</b><br>
            <br>
            <b>Locations</b>:<br>
            - Lazy Daze <a href="https://goo.gl/maps/yG57sXc9Mt3jaQMr8">4416 Fairmont Pkwy Ste 103, Pasadena, TX 77504</a><br>
            - Apartment <a href="https://goo.gl/maps/jQcvTGmZJdWFp3q16">18833 Town Ridge Ln, Webster, TX 77598</a><br>
            <br>
            Lazy Daze orders can be picked up <b>anytime during their business hours</b><br>
            <br>
            Time slots are limited, pick yours quickly
        </p>
        `;
    }
    else if (status_id == '3')  // Ready for Pickup
    {
        modal_title_text = `🚀 Ready for Pickup 🚀`;
        modal_body_text = 
        `
        <h4>Step 1: Select Pickup Day & Time</h4>
        <p style="font-size: smaller;">
            <b>Schedule Pickup</b>:<br>
            - Select your pick up day & time<br>
            - Select the location<br>
        </p>
        <h4>Step 2: Pickup At Location</h4>
            <p style="font-size: smaller;">
            <b>Directions for 77504 (Lazy Daze)</b>
            <br>
            <a href="https://goo.gl/maps/yG57sXc9Mt3jaQMr8">4416 Fairmont Pkwy Ste 103, Pasadena, TX 77504</a>
            <br>
            - Lazy Daze is avalible for pickup anytime within their business hours<br>
            <br>

            <b>Directions for 77598 (Apartment)</b>
            <br>
            <a href="https://goo.gl/maps/jQcvTGmZJdWFp3q16">18833 Town Ridge Ln, Webster, TX 77598</a>
            <br>
            - Do not enter the apartment complex<br>
            - Please park on either side of the street, closer to Retail Rd.<br>
            <br>
            Let me know you have arrived 👩‍🚀<br>
            <br>
            <b>Message me</b> here: <a href="https://twitter.com/DreamyDuoDesigns">dreamyduodesigns Twitter</a>
            <br>
        </p>
        `;
    }


    modal_title.innerHTML = modal_title_text;
    modal_body.innerHTML = modal_body_text;

    $("#modal1").modal('show');
}

function alertNotify(message, alertType, iconChoice, duration)
{
    if (iconChoice == 1)      // ✔
        iconChoice = 'check-circle-fill';
    else if (iconChoice == 2) // i
        iconChoice = 'info-fill';
    else if (iconChoice == 3) // !
        iconChoice = 'exclamation-triangle-fill';

    let iconHTML = `<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="${alertType}}:"><use xlink:href="#${iconChoice}"/></svg>`;
    alertType = `alert-${alertType}`;



    let html = 
    `
    <div id="alertNotification" class="alert ${alertType}  text-center  col-auto" style="margin: 0 auto; align-text: center;" role="alert">
        <span>
            ${iconHTML}
            ${message}
        </span>
    </div>
    `;

    // show pop up
    $('#notification').append(html);
    
    duration *= 1000;
    setTimeout(() =>
    {
        $( "#alertNotification" ).remove();
    }, duration);
}