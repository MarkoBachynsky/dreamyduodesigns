// var address = 'https://www.dreamyduodesigns.com';
var address        = 'http://localhost:8040';

let array_objects_shirts = Array.from(JSON.parse(document.getElementById('div_shirt_properties').getAttribute('data-shirt_properties')));
const button_add_to_cart = document.getElementById('button_add_to_cart');

// let object_shirt_properties = 
// {
//     string_name: 	name,
//     string_style: 	style,
//     string_color:	color,
//     string_size:	    size,
//     int_discount:    discount,
//     decimal_price:	price,
//     int_stock:		stock
// };

let decimal_price                 = array_objects_shirts[0].decimal_price;
// let selected_string_product    = array_objects_shirts[0].string_product;
// let selected_string_style      = array_objects_shirts[0].string_style;
// let selected_string_color      = array_objects_shirts[0].string_color;
// let selected_string_size       = array_objects_shirts[0].string_size;

let selected_int_id = selected_string_name = selected_string_product = selected_string_style = selected_string_color = selected_string_size = null;

let map_added_product = new Map();
let map_added_style   = new Map();
let map_added_color   = new Map();
let map_added_size    = new Map();

document.addEventListener('DOMContentLoaded', () =>
    {
        console.log( array_objects_shirts );

        // load user data
        fetch( address + '/getUserData' )
            .then( response => response.json() )
            .then( () =>
            {
                // Load Shirt Options
                html_load_shirt_options();
            } );
    });

function html_load_shirt_options()
{
    // load options based on prior base choices, for example: product > style > size > colors
    let div_product_radio_button_group = $('#div_product_radio_button_group');
    let div_style_radio_button_group   = $('#div_style_radio_button_group');
    let div_color_radio_button_group   = $('#div_color_radio_button_group');
    let div_size_radio_button_group    = $('#div_size_radio_button_group');

    div_product_radio_button_group.empty()
    div_style_radio_button_group.empty();
    div_color_radio_button_group.empty();
    div_size_radio_button_group.empty();

    map_added_product = new Map();
    map_added_style   = new Map();
    map_added_color   = new Map();
    map_added_size    = new Map();

    let html_product_radio_button = '';
    let html_style_radio_button   = '';
    let html_color_radio_button   = '';
    let html_size_radio_button    = '';

    let map_unavailable_styles = new Map();
    let map_unavailable_sizes  = new Map();
    let map_unavailable_colors = new Map();

    for (let i = 0; i < array_objects_shirts.length; i++)
    {
        let object_element_shirt = array_objects_shirts[i];
        // console.log(object_element_shirt);
        let string_property      = '';

        // if unique product not added, add it
        if (!map_added_product.has(object_element_shirt.string_product))
        {
            string_property         = 'product';
            let string_button_value = object_element_shirt.string_product;
            let string_input_id     = `radio_${string_property}_${string_button_value.toLowerCase()}`;
            let string_label_id     = `label_${string_property}_${string_button_value.toLowerCase()}`;

            if (selected_string_product == object_element_shirt.string_product)
            {
                html_product_radio_button += 
                `
                <input id="${string_input_id}" label_id="${string_label_id}" type="radio" class="btn-check" autocomplete="off" value="${string_button_value}" onclick="button_click_product(event)" name="options-outlined"> 
                <label id="${string_label_id}" for="${string_input_id}" class="btn btn-outline-primary active"  style="min-width: 68px;">${string_button_value}</label>
                `;
            }
            else if (selected_string_product !== object_element_shirt.string_product)
            {
                html_product_radio_button += 
                `
                <input id="${string_input_id}" label_id="${string_label_id}" type="radio" class="btn-check" autocomplete="off" value="${string_button_value}" onclick="button_click_product(event)" name="options-outlined"> 
                <label id="${string_label_id}" for="${string_input_id}" class="btn btn-outline-primary"  style="min-width: 68px;">${string_button_value}</label>
                `;
            }
            map_added_product.set(string_button_value, string_label_id);
        }

        // if unique style not added, add it
        if (!map_added_style.has(object_element_shirt.string_style))
        {
            string_property     = 'style';
            string_button_value = object_element_shirt.string_style;
            string_input_id     = `radio_${string_property}_${string_button_value.toLowerCase()}`;
            string_label_id     = `label_${string_property}_${string_button_value.toLowerCase()}`;

            // if option selected
            if (selected_string_style == object_element_shirt.string_style)
            {
                html_style_radio_button += 
                `
                <input id="${string_input_id}" label_id="${string_label_id}" type="radio" class="btn-check" autocomplete="off" value="${string_button_value}" onclick="button_click_style(event)" name="options-outlined"> 
                <label id="${string_label_id}" for="${string_input_id}" class="btn btn-outline-primary active"  style="min-width: 68px;">${string_button_value}</label>
                |`;
                map_added_style.set(string_button_value, string_label_id);
                map_unavailable_styles.delete(string_button_value);
            }
            // if option unselected, but available
            else if (selected_string_product == object_element_shirt.string_product)
            {
                html_style_radio_button += 
                `
                <input id="${string_input_id}" label_id="${string_label_id}" type="radio" class="btn-check" autocomplete="off" value="${string_button_value}" onclick="button_click_style(event)" name="options-outlined"> 
                <label id="${string_label_id}" for="${string_input_id}" class="btn btn-outline-primary"  style="min-width: 68px;">${string_button_value}</label>
                |`;
                map_added_style.set(string_button_value, string_label_id);
                map_unavailable_styles.delete(string_button_value);
            }
            // if option greyed out
            else
            {
                let html_unavailable_style_radio_button = 
                `
                <input id="${string_input_id}" label_id="${string_label_id}" type="radio" class="btn-check" autocomplete="off" value="${string_button_value}" onclick="button_click_style(event)" name="options-outlined"> 
                <label id="${string_label_id}" for="${string_input_id}" class="btn btn-outline-dark disabled"  style="min-width: 68px;">${string_button_value}</label>
                |`;
                map_unavailable_styles.set(string_button_value, html_unavailable_style_radio_button);
            }
        }

        // if unique size not added, add it
        if (!map_added_size.has(object_element_shirt.string_size))
        {
            string_property     = 'size';
            string_button_value = object_element_shirt.string_size;
            string_input_id     = `radio_${string_property}_${string_button_value.toLowerCase()}`;
            string_label_id     = `label_${string_property}_${string_button_value.toLowerCase()}`;

            // if option selected
            if (selected_string_size == object_element_shirt.string_size)
            {
                html_size_radio_button += 
                `
                <input id="${string_input_id}" label_id="${string_label_id}" type="radio" class="btn-check" autocomplete="off" value="${string_button_value}" onclick="button_click_size(event)" name="options-outlined"> 
                <label id="${string_label_id}" for="${string_input_id}" class="btn btn-outline-primary active"  style="min-width: 68px;">${string_button_value}</label>
                |`;
                map_added_size.set(string_button_value, string_label_id);
                map_unavailable_sizes.delete(string_button_value);
            }
            // if option unselected, but available
            else if (selected_string_product == object_element_shirt.string_product &&
                     selected_string_style   == object_element_shirt.string_style)
            {
                html_size_radio_button += 
                `
                <input id="${string_input_id}" label_id="${string_label_id}" type="radio" class="btn-check" autocomplete="off" value="${string_button_value}" onclick="button_click_size(event)" name="options-outlined"> 
                <label id="${string_label_id}" for="${string_input_id}" class="btn btn-outline-primary"  style="min-width: 68px;">${string_button_value}</label>
                |`;
                map_added_size.set(string_button_value, string_label_id);
                map_unavailable_sizes.delete(string_button_value);
            }
            // if option greyed out
            else
            {
                let html_unavailable_size_radio_button = 
                `
                <input id="${string_input_id}" label_id="${string_label_id}" type="radio" class="btn-check" autocomplete="off" value="${string_button_value}" onclick="button_click_size(event)" name="options-outlined"> 
                <label id="${string_label_id}" for="${string_input_id}" class="btn btn-outline-dark disabled"  style="min-width: 68px;">${string_button_value}</label>
                |`;
                map_unavailable_sizes.set(string_button_value, html_unavailable_size_radio_button);
            }
        }

        // if unique color not added, add it
        if (!map_added_color.has(object_element_shirt.string_color))
        {
            string_property     = 'color';
            string_button_value = object_element_shirt.string_color;
            string_input_id     = `radio_${string_property}_${string_button_value.toLowerCase()}`;
            string_label_id     = `label_${string_property}_${string_button_value.toLowerCase()}`;

            // if option selected
            if (selected_string_color == object_element_shirt.string_color)
            {
                html_color_radio_button += 
                `
                <input id="${string_input_id}" label_id="${string_label_id}" type="radio" class="btn-check" autocomplete="off" value="${string_button_value}" onclick="button_click_color(event)" name="options-outlined"> 
                <label id="${string_label_id}" for="${string_input_id}" class="btn btn-outline-primary active"  style="min-width: 68px;">${string_button_value}</label>
                |`;
                map_added_color.set(string_button_value, string_label_id);
                map_unavailable_colors.delete(string_button_value);
            }
            // if option unselected, but available
            else if (selected_string_product == object_element_shirt.string_product &&
                     selected_string_style   == object_element_shirt.string_style   &&
                     selected_string_size    == object_element_shirt.string_size)
            {
                html_color_radio_button += 
                `
                <input id="${string_input_id}" label_id="${string_label_id}" type="radio" class="btn-check" autocomplete="off" value="${string_button_value}" onclick="button_click_color(event)" name="options-outlined"> 
                <label id="${string_label_id}" for="${string_input_id}" class="btn btn-outline-primary"  style="min-width: 68px;">${string_button_value}</label>
                |`;
                map_added_color.set(string_button_value, string_label_id);
                map_unavailable_colors.delete(string_button_value);
            }
            // if option greyed out
            else
            {
                let html_unavailable_color_radio_button = 
                `
                <input id="${string_input_id}" label_id="${string_label_id}" type="radio" class="btn-check" autocomplete="off" value="${string_button_value}" onclick="button_click_color(event)" name="options-outlined"> 
                <label id="${string_label_id}" for="${string_input_id}" class="btn btn-outline-dark disabled"  style="min-width: 68px;">${string_button_value}</label>
                |`;
                map_unavailable_colors.set(string_button_value, html_unavailable_color_radio_button);
            }
        }
    }

    for (let [key, value] of map_unavailable_styles) { html_style_radio_button += value; }
    for (let [key, value] of map_unavailable_sizes)  { html_size_radio_button  += value; }
    for (let [key, value] of map_unavailable_colors) { html_color_radio_button += value; }


    // extra specific sorting for styles: Unisex, Men, Women
    let array_html_style_radio_button = html_style_radio_button.split('|');
    html_style_radio_button = '';

    // extra specific sorting for sizes: XS, S, M, L, XL, 2XL
    let array_html_size_radio_button = html_size_radio_button.split('|');
    html_size_radio_button = '';

    // extra specific sorting for colors: black, blue, green, red, white
    let array_html_color_radio_button = html_color_radio_button.split('|').sort();
    html_color_radio_button = '';

    let string_html_style_unisex = string_html_style_men = string_html_style_women = '';
    let string_html_size_XS = string_html_size_S = string_html_size_M = string_html_size_L = string_html_size_XL = string_html_size_2XL = '';

    // styles
    for (let i = 0; i < array_html_style_radio_button.length; i++)
    {
        let string_html = array_html_style_radio_button[i];

        if (string_html.toLocaleLowerCase().includes('radio_style_unisex')) string_html_style_unisex = string_html;
        if (string_html.toLocaleLowerCase().includes('radio_style_men')) string_html_style_men = string_html;
        if (string_html.toLocaleLowerCase().includes('radio_style_women')) string_html_style_women = string_html;
    }

    html_style_radio_button += string_html_style_unisex;
    html_style_radio_button += string_html_style_men;
    html_style_radio_button += string_html_style_women;

    // sizes
    for (let i = 0; i < array_html_size_radio_button.length; i++)
    {
        let string_html = array_html_size_radio_button[i];
        if (string_html.toLocaleLowerCase().includes('radio_size_xs')) string_html_size_XS = string_html;
        if (string_html.toLocaleLowerCase().includes('radio_size_s'))  string_html_size_S = string_html;
        if (string_html.toLocaleLowerCase().includes('radio_size_m'))  string_html_size_M = string_html;
        if (string_html.toLocaleLowerCase().includes('radio_size_l'))  string_html_size_L = string_html;
        if (string_html.toLocaleLowerCase().includes('radio_size_xl'))  string_html_size_XL = string_html;
        if (string_html.toLocaleLowerCase().includes('radio_size_2xl'))  string_html_size_2XL = string_html;
    }

    // html_size_radio_button += string_html_size_XS;
    html_size_radio_button += string_html_size_S;
    html_size_radio_button += string_html_size_M;
    html_size_radio_button += string_html_size_L;
    html_size_radio_button += string_html_size_XL;
    html_size_radio_button += string_html_size_2XL;

    // colors
    for (let i = 0; i < array_html_color_radio_button.length; i++)
    {
        let string_html = array_html_color_radio_button[i];
        html_color_radio_button += string_html;
    }

    // apply html to page
    div_product_radio_button_group.append(html_product_radio_button);
    div_style_radio_button_group.append(html_style_radio_button);
    div_size_radio_button_group.append(html_size_radio_button);
    div_color_radio_button_group.append(html_color_radio_button);

}

function button_click_product(event)
{
    let html_button_clicked = event.target;
    let string_label_id = html_button_clicked.getAttribute("label_id");

    // document.getElementById("radio_t-shirt").classList.remove("active"); 
    // document.getElementById("radio_long_sleeve").classList.remove("active"); 
    // document.getElementById("radio_sweatshirt").classList.remove("active");
    
    for (let [key, value] of map_added_product)
    {
        document.getElementById(value).classList.remove("active"); 
    }

    document.getElementById(string_label_id).classList.add("active");
    selected_string_product = html_button_clicked.value;
    selected_string_style = selected_string_size = selected_string_color  = null;
    html_load_shirt_options();
    button_add_to_cart.classList.add("disabled");
    button_add_to_cart.classList.add("btn-outline-dark");
    button_add_to_cart.classList.remove("btn-outline-primary");
}

function button_click_style(event)
{
    let html_button_clicked = event.target;
    let string_label_id = html_button_clicked.getAttribute("label_id");

    for (let [key, value] of map_added_style)
    {
        document.getElementById(value).classList.remove("active"); 
    }

    document.getElementById(string_label_id).classList.add("active");
    selected_string_style = html_button_clicked.value;
    selected_string_size  = selected_string_color = null;
    html_load_shirt_options();
    button_add_to_cart.classList.add("disabled");
    button_add_to_cart.classList.add("btn-outline-dark");
    button_add_to_cart.classList.remove("btn-outline-primary");
}

function button_click_size(event)
{
    let html_button_clicked = event.target;
    let string_label_id = html_button_clicked.getAttribute("label_id");

    for (let [key, value] of map_added_size)
    {
        document.getElementById(value).classList.remove("active"); 
    }

    document.getElementById(string_label_id).classList.add("active");
    selected_string_size  = html_button_clicked.value;
    selected_string_color = null;
    html_load_shirt_options();
    button_add_to_cart.classList.add("disabled");
    button_add_to_cart.classList.add("btn-outline-dark");
    button_add_to_cart.classList.remove("btn-outline-primary");
}

function button_click_color(event)
{
    let html_button_clicked = event.target;
    let string_label_id = html_button_clicked.getAttribute("label_id");

    for (let [key, value] of map_added_color)
    {
        document.getElementById(value).classList.remove("active"); 
    }

    document.getElementById(string_label_id).classList.add("active");
    selected_string_color = html_button_clicked.value;
    html_load_shirt_options();
    button_add_to_cart.classList.remove("disabled");
    button_add_to_cart.classList.remove("btn-outline-dark");
    button_add_to_cart.classList.add("btn-outline-primary");
}

function button_click_add_to_cart()
{

    // get id for selected shirt variation
    for (let i = 0; i < array_objects_shirts.length; i++)
    {
        let object_shirt_element = array_objects_shirts[i];
        console.log(object_shirt_element);
        if ((object_shirt_element.string_product == selected_string_product) &&
            (object_shirt_element.string_style   == selected_string_style)   &&
            (object_shirt_element.string_color   == selected_string_color)   &&
            (object_shirt_element.string_size    == selected_string_size))
        {
            // console.log(true);
            selected_string_name = object_shirt_element.string_name;
            selected_int_id = object_shirt_element.int_id;
            break;
        }
    }

    console.table([['selected_int_id', selected_int_id], ['selected_string_product', selected_string_product], ['selected_string_style', selected_string_style], ['selected_string_color', selected_string_color], ['selected_string_size', selected_string_size]]);


    fetch(address + '/cartAddItem',
    {
        credentials: "include",
        method: 'PATCH',
        headers:
        {
            'Content-type': 'application/json'
        },
        body: JSON.stringify(
        {
            itemId: selected_int_id,
            itemQty: 1
        })
    })
    .then(response => response.json())
    .then((cart) => 
    {
        var total = cart[0][1];
        // console.log(cart);

        // console.log(cart.data.cart);
        var cartQty = document.getElementById('cart-quantity');
        cartQty.dataset.quantity = total;
        $("#cart-quantity").text(total);
        
        // Notification
        const message       = `${selected_string_name} added`;
        const alertType     = 'success';
        const iconChoice    = 1;
        alertNotify(message, alertType, iconChoice, 2);
    }).catch((error => 
    {
        console.log("button_click_add_to_cart()  catch:" + error);
    }));
}

function alertNotify(message, alertType, iconChoice, duration)
{
    if (iconChoice == 1)      // ✔
        iconChoice = 'check-circle-fill';
    else if (iconChoice == 2) // i
        iconChoice = 'info-fill';
    else if (iconChoice == 3) // !
        iconChoice = 'exclamation-triangle-fill';

    let iconHTML = `<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="${alertType}}:"><use xlink:href="#${iconChoice}"/></svg>`;
    alertType = `alert-${alertType}`;

    let html = 
    `
    <div id="alertNotification" class="alert ${alertType}  text-center  col-auto" style="margin: 0 auto; align-text: center;" role="alert">
        <span>
            ${iconHTML}
            ${message}
        </span>
    </div>
    `;

    // show pop up
    $('#notification').append(html);
    
    duration *= 1000;
    setTimeout(() =>
    {
        $( "#alertNotification" ).remove();
    }, duration);
}