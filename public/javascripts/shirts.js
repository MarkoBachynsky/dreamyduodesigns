// var address = 'https://www.dreamyduodesigns.com';
var address = 'http://localhost:8040';


document.addEventListener('DOMContentLoaded', () =>
    {
        // load user data
        fetch( address + '/getUserData' )
        .then( response => response.json() )
        .then( data => loadCartTotal( data[ 'data' ] ) )
        .then( () =>
        {
            // load menu data
            fetch( address + '/get_table_shirts' )
                .then( response => response.json() )
                .then( data => loadMenuCards( data[ 'data' ] ) );
        } );
    });


function loadMenuCards(data)
{
    try
    {
        if (data.length == 0)
        {
            console.log("data.length == 0");
            return;
        }
    } catch (error)
    {
        console.log(error);
    }

    let myform = $('#myform');
    let map_shirts = new Map();
    // Array.from(data).forEach(function ({ id, name, price, description, stock })
    Array.from(data).forEach(( { id, name, style, color, size, discount, price, stock } ) =>
    {
        // console.log(id + "\t" + name + "\t" + price + "\t" + description);
        // if map does not have unique shirt name, add it
        if ( !map_shirts.has( name ) )
        {
            let array_array_new_shirt = [ [ style, color, size, discount, price, stock ] ];
            map_shirts.set( name, array_array_new_shirt );
        }

        else
        {
            let array_array_existing_shirt = map_shirts.get( name );
            array_array_existing_shirt.push( [ style, color, size, discount, price, stock ] );
        }


    });

    for (let [key, value] of map_shirts)
    {
        console.log([key, value]);
        let string_name = key;
        let string_name_url = 'shirt/' + string_name.replace(/ /g, '_');
        let card = "";

        // <div name="card" class="card  d-flex align-items-start flex-column " ${dataAttributes} style="max-width: 304px" >
        // <img class="card-img-top" src="../images/${name}.jpg"  alt="...">


        // <div class="card-body" style="font-size: 0.75em; width: 100%;">
        //     <p class="card-text">${description}</p>
        // </div>

        card +=
            `
            <div class="col-6 col-md-4 col-lg-3 d-flex  text-center">
                <div name="card" class="card w-100 d-flex align-items-start flex-column " style="max-width: 304px" >

                    <div class="card-header" style="width: 100%;">
                        <h5 class="card-title">
                            <b>${string_name}</b>
                        </h5>
                    </div>

                    <img class="card-img-top" src="../images/shirt.jpg" style="border-radius: 0%; max-width: 240px; max-height: 240px; margin: auto;" alt="..." loading="lazy" >

                    <div class="card-body" style="font-size: 0.75em; width: 100%;">
                        <p class="card-text">{description}</p>
                    </div>

                    <div class="card-footer" style="width: 100%; height: 90px;">
                        <h6 class="card-text user-select-none" style="font-size: normal;" >$25.00</h6>
                        <!-- <p class="card-text" style="font-size: small;" >(%{stock}) in stock</p> -->
                        
                        <a href="/${string_name_url}">
                            <button name="shop-item-button" class="btn btn-primary rounded-pill" type="button" style="width: 100%;">Add to cart</button>
                        </a>
                    </div>

                </div>
            </div>
        `;
        myform.append(card);
    }
    // console.log("buttons created");

    // create menu buttons
    // let removeCartItemButtons = document.getElementsByClassName('btn-danger');
    // for (let i = 0; i < removeCartItemButtons.length; i++)
    // {
    //     let button = removeCartItemButtons[i];
    //     button.addEventListener('click', removeCartItem);
    // }

    // let quantityInputs = document.getElementsByClassName('cart-quantity-input');
    // for (let i = 0; i < quantityInputs.length; i++)
    // {
    //     let input = quantityInputs[i];
    //     input.addEventListener('change', quantityChanged);
    // }

    // // console.log("shop-item-button Search Start");
    // let addToCartButtons = document.getElementsByName('shop-item-button');
    // for (let i = 0; i < addToCartButtons.length; i++)
    // {
    //     // console.log("button found!");
    //     let button = addToCartButtons[i];
    //     button.addEventListener('click', addToCartClicked);
    //     // console.log("button " + i + " online!");
    // }
    // // console.log("shop-item-button Search End");

}

// Update Cart Quantity
function loadCartTotal(data)
{
    try
    {
        // get total of items 
        let cart        = data.cart[0][1];
        // let cart_points = data.cart_points.cart[0][1];

        let totalQty = cart;

        if (data == null)
        {
            console.log('Error: No User Data');
            return;
        }

        // navbar
        let cartQty = document.getElementById('cart-quantity');
        cartQty.dataset.quantity = totalQty;
        $("#cart-quantity").text(totalQty);
    }
    catch (error)
    {
        console.log(error);
    }
}

function addToCartClicked(event)
{
    // console.log("\n" + "addToCartClicked()");
    let button = event.target;
    let id = parseInt(button.dataset.id);
    let title = button.dataset.name;
    let price = parseFloat(button.dataset.price);

    // console.log(shopItem.dataAttributes);
    // console.log("id:\t" + id);
    // console.log("title:\t" + title);
    // console.log("price:\t" + price);

    fetch(address + '/cartAddItem',
        {
            credentials: "include",
            method: 'PATCH',
            headers:
            {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(
                {
                    itemId: id,
                    itemQty: 1
                })
        })
        .then(response => response.json())
        .then((data) => 
        {
            let cart = data['data'].cart;
            let total = cart[0][1];
            // console.log(cart);

            // console.log(cart.data.cart);
            let cartQty = document.getElementById('cart-quantity');
            cartQty.dataset.quantity = total;
            $("#cart-quantity").text(total);

            
            // Notification
            const message = `${title} added`;
            const alertType     = 'success';
            const iconChoice    = 1;
            alertNotify(message, alertType, iconChoice, 2);

            // console.log("addToCartClicked complete");
        }).catch((error => 
        {
            console.log("addToCartClicked(event)  catch:" + error);
        }));
}

function alertNotify(message, alertType, iconChoice, duration)
{
    if (iconChoice == 1)      // ✔
        iconChoice = 'check-circle-fill';
    else if (iconChoice == 2) // i
        iconChoice = 'info-fill';
    else if (iconChoice == 3) // !
        iconChoice = 'exclamation-triangle-fill';

    let iconHTML = `<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="${alertType}}:"><use xlink:href="#${iconChoice}"/></svg>`;
    alertType = `alert-${alertType}`;

    let html = 
    `
    <div id="alertNotification" class="alert ${alertType}  text-center  col-auto" style="margin: 0 auto; align-text: center;" role="alert">
        <span>
            ${iconHTML}
            ${message}
        </span>
    </div>
    `;

    // show pop up
    $('#notification').append(html);
    
    duration *= 1000;
    setTimeout(() =>
    {
        $( "#alertNotification" ).remove();
    }, duration);
}