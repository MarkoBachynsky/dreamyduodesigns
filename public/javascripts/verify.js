
var address = 'https://www.dreamyduodesigns.com';
// var address = 'http://localhost:8080';

function submit_verification_code()
{
    var verification_code = $('#inputCode').val();
    verification_code = $.trim(verification_code);

    fetch(address + '/submit_verification_code',
    {
        credentials: "include",
        method: 'PATCH',
        headers:
        {
            'Content-type': 'application/json'
        },
        body: JSON.stringify
            ({
                verification_code: verification_code
            })
    })
    .then(response => response.json())
    .then((result) => 
    {
        if (result == true)
        {
            // Notification
            const message       = 'Account Verified'; 
            const alertType     = 'success';
            const iconChoice    = 3;
            alertNotify(message, alertType, iconChoice, 3);


            // this will automatically close the alert in 2 secs
            

            var duration = 3 * 1000;
            setTimeout(() =>
            { // this will automatically close the alert in 3 secs
                window.location.replace('/menu');
            }, duration);
        }
        else if (result == false)
        {
            // Notification
            const message       = 'Error: Code does not match'; 
            const alertType     = 'danger';
            const iconChoice    = 3;
            alertNotify(message, alertType, iconChoice, 3);
        }
    }).catch((error => 
    {
        console.log("submit_verification_code  catch:" + error);

        // Notification
        const message       = 'Error: Code does not match'; 
        const alertType     = 'danger';
        const iconChoice    = 3;
        alertNotify(message, alertType, iconChoice, 3);
    }));
}

function alertNotify(message, alertType, iconChoice, duration)
{
    if (iconChoice == 1)      // ✔
        iconChoice = 'check-circle-fill';
    else if (iconChoice == 2) // i
        iconChoice = 'info-fill';
    else if (iconChoice == 3) // !
        iconChoice = 'exclamation-triangle-fill';

    var iconHTML = `<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="${alertType}}:"><use xlink:href="#${iconChoice}"/></svg>`;
    alertType = `alert-${alertType}`;

    var html = 
    `
    <div id="alertNotification" class="alert ${alertType}  text-center  col-auto" style="margin: 0 auto; align-text: center;" role="alert">
        <span>
            ${iconHTML}
            ${message}
        </span>
    </div>
    `;

    // show pop up
    $('#notification').append(html);
    
    duration *= 1000;
    setTimeout(() =>
    {
        $( "#alertNotification" ).remove();
    }, duration);
}